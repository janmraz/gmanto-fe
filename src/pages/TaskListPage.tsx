import React, { Component, RefObject } from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { taskListStore } from '../stores/TaskListStore';
import TaskList from '../model/TaskList';
import { tasksStore } from '../stores/TasksStore';
import { observer } from 'mobx-react';
import { projectStore } from '../stores/ProjectsStore';
import TaskListComponent from '../components/TaskListComponent';
import Task from '../model/Task';
import Nav from '../components/Nav';
import NotificationComponent from '../components/NotificationsComponent';
import ModalComponent from '../components/ModalComponent';
import User from '../model/User';
import Utils from '../Utils';
import { userStore } from '../stores/UsersStore';
import { commentsStore } from '../stores/CommentsStore';
import Comment from '../model/Comment';
import { DragDropContext, Droppable, DropResult, ResponderProvided } from 'react-beautiful-dnd';
import ProjectToUser from '../model/ProjectToUser';

interface TaskListPageState {
	taskLists: TaskList[];
	projectName: string;
	projectId: number;
	openArchived: boolean;
	openModal: boolean;
	openNew: boolean;
	peopleInProject: ProjectToUser[];
	projectOwner: User | null;
	taskListName: string;
	notifications: Comment[];
	currentNotificationsSkip: number;
	loading: boolean;
}

@observer
class TaskListPage extends Component<RouteComponentProps<any>, TaskListPageState> {
	async componentDidMount(): Promise<void> {
		await this.reloadData();
	}
	constructor(props: RouteComponentProps<any>) {
		super(props);
		this.textInput = React.createRef();
		this.setTitle();

		this.state = {
			taskLists: [],
			projectName: '',
			projectId: Utils.VOID_ID,
			openArchived: false,
			openModal: false,
			openNew: false,
			peopleInProject: [],
			projectOwner: null,
			taskListName: '',
			notifications: [],
			currentNotificationsSkip: 0,
			loading: true,
		};
	}

	textInput: RefObject<HTMLInputElement>;

	setTitle = (nots?: number, projectName?: string) => {
		if (nots) {
			document.title = projectName ? '(' + nots + ') Gmanto - ' + projectName : '(' + nots + ') Gmanto';
		} else {
			document.title = projectName ? 'Gmanto - ' + projectName : 'Gmanto';
		}
	};

	reloadData = async () => {
		try {
			this.setState({ loading: true });
			await Promise.all([
				taskListStore.fetchAllTaskLists(this.props.match.params.id).then(() => this.setState({ taskLists: taskListStore.taskLists })),
				projectStore.fetchProject(this.props.match.params.id).then(project =>
					this.setState({
						projectName: project.name,
						projectId: project.id,
						peopleInProject: project.collaborators,
					})
				),
				commentsStore.getCommentInfo(parseInt(this.props.match.params.id), 0, Utils.NOTIFICATIONS_BATCH).then(receivedNotifications => {
					const newNotifications = this.state.notifications;
					receivedNotifications.forEach(c => {
						if (newNotifications.findIndex(n => n.id === c.id) < 0) {
							newNotifications.push(c);
						} else {
							const index = newNotifications.findIndex(n => n.id === c.id);
							newNotifications[index] = c;
						}
					});
					this.setState({ notifications: newNotifications });
				}),
			]);

			// notifications title update
			let countNotifications = 0;
			taskListStore.taskLists.forEach(tsklt =>
				tsklt.tasks.forEach(tsk =>
					tsk.comments.forEach(c => {
						if (!c.seen) {
							countNotifications++;
						}
					})
				)
			);
			this.setTitle(countNotifications, this.state.projectName);
			this.setState({ loading: false });
		} catch (e) {
			console.error(e);
			if (e && e.response && e.response.status === 401) {
				window.location.href = '/projects/' + this.props.match.params.id + '/access-denied';
			}
		}
	};

	fetchMoreNotifications = async () => {
		const receivedNotifications = await commentsStore.getCommentInfo(
			parseInt(this.props.match.params.id),
			this.state.currentNotificationsSkip,
			Utils.NOTIFICATIONS_BATCH
		);
		const newNotifications = this.state.notifications;
		receivedNotifications.forEach(c => {
			if (newNotifications.findIndex(n => n.id === c.id) < 0) {
				newNotifications.push(c);
			}
		});
		this.setState({ notifications: newNotifications });
		commentsStore.setSeen(this.state.projectId, receivedNotifications).then(() => {
			this.setState({
				notifications: newNotifications.map(n => {
					n.seen = true;
					return n;
				}),
			});
			this.reloadData();
		});
	};

	_handleKeyDown = (e: any) => {
		if (e.key === 'Enter' && this.state.taskListName !== '') {
			this.createTaskList();
		}
	};

	createTaskList = async () => {
		if (this.state.taskListName !== '') {
			const taskList = {
				name: this.state.taskListName,
				tasks: [],
				archived: false,
				id: Utils.VOID_ID,
				order: Utils.DEFAULT_ORDER,
			};
			this.setState({ taskListName: '', taskLists: this.state.taskLists.concat([taskList]) });

			await taskListStore.createTaskList(this.props.match.params.id, taskList);
			await this.reloadData();
		}
	};

	updateTaskList = async (taskList: TaskList) => {
		const taskLists = this.state.taskLists;
		const taskListIndex = taskLists.findIndex(tsk => tsk.id === taskList.id);
		taskLists[taskListIndex] = taskList;
		this.setState({ taskLists });

		await taskListStore.editTaskList(this.props.match.params.id, taskList);
		this.reloadData();
	};

	deleteTaskList = async (taskList: TaskList) => {
		if (window.confirm('Are you sure you wish to delete this taskList?')) {
			this.setState({ taskLists: this.state.taskLists.filter(tsk => tsk.id === taskList.id) });

			await taskListStore.removeTaskList(this.props.match.params.id, taskList);
			this.reloadData();
		}
	};

	createTask = async (name: string, taskListId: number) => {
		const taskLists = this.state.taskLists;
		const taskListIndex = taskLists.findIndex(tsk => tsk.id === taskListId);
		const task = {
			name: name,
			done: false,
			id: Utils.VOID_ID,
			comments: [],
			dateDue: null,
			dateCreated: new Date(),
			assignee: null,
			followers: [userStore.user!],
			order: Utils.DEFAULT_ORDER,
		};
		taskLists[taskListIndex].tasks.push(task);
		this.setState({ taskLists });

		await tasksStore.createTask(taskListId, task);
		this.reloadData();
	};

	toggledTask = async (task: Task, taskListId: number, value: boolean) => {
		const taskLists = this.state.taskLists;
		const taskListIndex = taskLists.findIndex(tsk => tsk.id === taskListId);
		const taskIndex = taskLists[taskListIndex].tasks.findIndex(t => t.id === task.id);
		task.done = value;
		taskLists[taskListIndex].tasks[taskIndex] = task;
		this.setState({ taskLists });

		if (value) {
			await tasksStore.doneTask(task, taskListId);
		} else {
			await tasksStore.undoneTask(task, taskListId);
		}
		await this.reloadData();
		this.forceUpdate();
	};

	invite = async (email: string) => {
		await projectStore.addCollaborator(this.state.projectId, email);
		await this.reloadData();
	};

	inviteCallback = async () => {
		this.setState({ openModal: true });
	};

	changedDeadline = async (task: Task, taskList: TaskList, date: Date | null) => {
		const taskLists = this.state.taskLists;
		const taskListIndex = taskLists.findIndex(tsk => tsk.id === taskList.id);
		const taskIndex = taskLists[taskListIndex].tasks.findIndex(t => t.id === task.id);
		task.dateDue = date;
		taskLists[taskListIndex].tasks[taskIndex] = task;
		this.setState({ taskLists });

		await tasksStore.changeDeadline(task, taskList.id);
		await this.reloadData();
	};

	changedAssignee = async (task: Task, taskList: TaskList, user: User | null) => {
		const taskLists = this.state.taskLists;
		const taskListIndex = taskLists.findIndex(tsk => tsk.id === taskList.id);
		const taskIndex = taskLists[taskListIndex].tasks.findIndex(t => t.id === task.id);
		task.assignee = user;
		taskLists[taskListIndex].tasks[taskIndex] = task;
		this.setState({ taskLists });

		await tasksStore.changeAssignee(task, taskList.id);
		await this.reloadData();
	};

	changeOwnership = async (user: User) => {
		this.setState({
			peopleInProject: this.state.peopleInProject.map(p => {
				if (p.owner) {
					p.owner = false;
				}
				if (p.user.id === user.id) {
					p.owner = true;
				}
				return p;
			}),
		});

		await projectStore.changeOwner(this.state.projectId, user);
		await this.reloadData();
	};

	removeUser = async (user: User) => {
		if (window.confirm('Are you sure you wish to remove this collaborator?')) {
			this.setState({ peopleInProject: this.state.peopleInProject.filter(p => p.user.id !== user.id) });

			await projectStore.removeCollaborator(this.state.projectId, user);
			await this.reloadData();
		}
	};

	onOpen = async () => {
		this.setState({
			notifications: this.state.notifications.map(n => {
				n.seen = true;
				return n;
			}),
		});

		await commentsStore.setSeen(this.state.projectId, this.state.notifications);
		await this.reloadData();
	};

	onClickedMore = async () => {
		await this.setState({ currentNotificationsSkip: this.state.currentNotificationsSkip + Utils.NOTIFICATIONS_BATCH });
		await this.fetchMoreNotifications();
	};

	onDragEnd = async (data: DropResult, provided: ResponderProvided) => {
		if (!data.destination) {
			return;
		}
		if (data.destination && data.source.droppableId !== data.destination.droppableId) {
			const taskId = parseInt(Utils.stripPrefixFromString(data.draggableId));
			const taskLists = this.state.taskLists;
			const sourceTaskListIndex = taskLists.findIndex(t => t.tasks.some(task => task.id === taskId));
			const task = taskLists[sourceTaskListIndex].tasks.find(t => t.id === taskId);
			taskLists[sourceTaskListIndex].tasks = taskLists[sourceTaskListIndex].tasks.filter(t => t.id !== taskId);

			const destinationTaskListId = parseInt(Utils.stripPrefixFromString(data.destination?.droppableId));
			const destinationTaskListIndex = taskLists.findIndex(tsk => tsk.id === destinationTaskListId);
			taskLists[destinationTaskListIndex].tasks.push(task!);
			task!.order = data.destination.index;

			const originalArr = taskLists[destinationTaskListIndex].tasks.filter(task => !task.done).sort((a, b) => a.order - b.order);
			const originalPosition = originalArr.findIndex(t => t.id === taskId);
			taskLists[destinationTaskListIndex].tasks = this.reorderTasks(
				{ newIndex: data.destination.index, oldIndex: originalPosition },
				originalArr
			);
			this.setState({ taskLists });

			await tasksStore.moveTask(destinationTaskListId, taskId, data.destination.index);
			await this.reloadData();
		} else {
			if (data.type === 'tasks') {
				const taskId = parseInt(Utils.stripPrefixFromString(data.draggableId));
				const taskLists = this.state.taskLists;
				const taskListIndex = taskLists.findIndex(t => t.tasks.some(task => task.id === taskId));
				const originalArr = taskLists[taskListIndex].tasks.filter(task => !task.done).sort((a, b) => a.order - b.order);
				const originalPosition = originalArr.findIndex(t => t.id === taskId);
				taskLists[taskListIndex].tasks = this.reorderTasks({ newIndex: data.destination.index, oldIndex: originalPosition }, originalArr);
				this.setState({ taskLists });

				await tasksStore.changeOrder(this.state.taskLists[taskListIndex].id, taskId, data.destination.index);
				await this.reloadData();
			} else {
				const taskListId = parseInt(Utils.stripPrefixFromString(data.draggableId));
				const originalArr = this.state.taskLists.filter(p => !p.archived).sort((a, b) => a.order - b.order);
				const originalPosition = originalArr.findIndex(t => t.id === taskListId);
				const newArr = this.reorderTaskLists({ newIndex: data.destination.index, oldIndex: originalPosition }, originalArr);
				this.setState({ taskLists: newArr });

				await taskListStore.changeOrderTaskList(this.state.projectId, taskListId, data.destination.index);
				await this.reloadData();
			}
		}
	};

	reorderTaskLists = (event: { newIndex: number; oldIndex: number }, originalArray: TaskList[]) => {
		const movedItem = originalArray.filter((item, index) => index === event.oldIndex);
		const remainingItems = originalArray.filter((item, index) => index !== event.oldIndex);

		const reorderedItems = [...remainingItems.slice(0, event.newIndex), movedItem[0], ...remainingItems.slice(event.newIndex)];

		return reorderedItems.map((c, i) => {
			c.order = i;
			return c;
		});
	};

	reorderTasks = (event: { newIndex: number; oldIndex: number }, originalArray: Task[]) => {
		const movedItem = originalArray.filter((item, index) => index === event.oldIndex);
		const remainingItems = originalArray.filter((item, index) => index !== event.oldIndex);

		const reorderedItems = [...remainingItems.slice(0, event.newIndex), movedItem[0], ...remainingItems.slice(event.newIndex)];

		return reorderedItems.map((c, i) => {
			c.order = i;
			return c;
		});
	};

	render() {
		const notArchivedTaskList = this.state.taskLists.filter(p => !p.archived).sort((a, b) => a.order - b.order);
		return (
			<div>
				{this.state.loading && <div className="loading loading--active"></div>}
				<Nav
					projectsNav={false}
					onShare={() => this.setState({ openModal: true })}
					peopleInProject={this.state.peopleInProject}
					history={this.props.history}
				/>
				<NotificationComponent
					projectId={parseInt(this.props.match.params.id)}
					notifications={this.state.notifications}
					onOpen={this.onOpen}
					onClickMore={this.onClickedMore}
				/>
				<ModalComponent
					open={this.state.openModal}
					onClose={() => this.setState({ openModal: false })}
					invite={this.invite}
					peopleInProject={this.state.peopleInProject}
					projectOwner={this.state.projectOwner}
					removeUser={this.removeUser}
					makeOwner={this.changeOwnership}
				/>
				<main className="container">
					<div className="top">
						<Link to="/" className="btnset">
							<span className="btnset__box"></span>
							<span className="btnset__text">Projects</span>
						</Link>
						<div className="headline">{this.state.projectName}</div>
					</div>

					<div className="content">
						<DragDropContext onDragEnd={this.onDragEnd}>
							<Droppable droppableId={'tasklists'} type="TASKLISTS">
								{(provided, snapshot) => (
									<div ref={provided.innerRef} {...provided.droppableProps}>
										{notArchivedTaskList.map((taskList, index) => (
											<TaskListComponent
												key={taskList.id}
												taskList={taskList}
												index={index}
												peopleInProject={this.state.peopleInProject}
												updateTaskList={this.updateTaskList}
												createTask={this.createTask}
												deleteTaskList={this.deleteTaskList}
												toggledTask={this.toggledTask}
												changedDeadline={this.changedDeadline}
												changedAssignee={this.changedAssignee}
												inviteUser={this.inviteCallback}
												{...this.props}
											/>
										))}
										{provided.placeholder}
									</div>
								)}
							</Droppable>
						</DragDropContext>

						{(this.state.openNew || this.state.taskLists.length === 0) && (
							<div className="tasklist">
								<div className="tasklist__top">
									<input
										type="text"
										value={this.state.taskListName}
										ref={this.textInput}
										autoFocus={true}
										onChange={event => this.setState({ taskListName: event.target.value })}
										onKeyDown={this._handleKeyDown}
										className="tasklist__new__input"
										placeholder="Enter name of this tasklist"
									/>
								</div>
							</div>
						)}

						<div className="tasklist__btns">
							<button
								className={this.state.openNew ? 'btn btn--add-sec' : 'btn'}
								onClick={() => {
									this.setState({ openNew: !this.state.openNew, taskListName: '' });
									if (this.textInput.current) {
										this.textInput.current.focus();
									}
								}}>
								Add new tasklist
							</button>
							{this.state.taskLists.filter(p => p.archived).length !== 0 && (
								<button className="btn btn--blank" onClick={() => this.setState({ openArchived: !this.state.openArchived })}>
									{this.state.openArchived ? 'Hide archived tasklists' : 'Show archived tasklists'}
								</button>
							)}
						</div>

						{this.state.openArchived && (
							<div className="content">
								<DragDropContext onDragEnd={() => ({})}>
									<Droppable droppableId={'tasklists'} type="TASKLISTS">
										{(provided, snapshot) => (
											<div ref={provided.innerRef} {...provided.droppableProps}>
												{this.state.taskLists
													.filter(p => p.archived)
													.map((taskList, index) => (
														<TaskListComponent
															key={taskList.id}
															taskList={taskList}
															index={index}
															peopleInProject={this.state.peopleInProject}
															updateTaskList={this.updateTaskList}
															createTask={this.createTask}
															deleteTaskList={this.deleteTaskList}
															toggledTask={this.toggledTask}
															changedDeadline={this.changedDeadline}
															changedAssignee={this.changedAssignee}
															inviteUser={this.inviteCallback}
															{...this.props}
														/>
													))}
											</div>
										)}
									</Droppable>
								</DragDropContext>
							</div>
						)}
					</div>
				</main>
			</div>
		);
	}
}

export default TaskListPage;
