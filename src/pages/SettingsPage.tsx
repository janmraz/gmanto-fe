import React, { Component, RefObject } from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { observer } from 'mobx-react';
import Nav from '../components/Nav';
import { userStore } from '../stores/UsersStore';
import User from '../model/User';
import Utils from '../Utils';
import UserPhoto from '../components/UserPhotoComponent';

interface SettingsPageState {
	uploading: boolean;
}
@observer
class SettingsPage extends Component<RouteComponentProps<any>, User & SettingsPageState> {
	async componentDidMount(): Promise<void> {
		await this.reloadData();
	}

	constructor(props: RouteComponentProps<any>) {
		super(props);
		this.textInput = React.createRef();
		this.photoInput = React.createRef();
		document.title = "Gmanto - Settings";

		this.state = {
			firstName: '',
			lastName: '',
			notificationOn: false,
			email: '',
			darkMode: false,
			profileImgUrl: '',
			id: Utils.VOID_ID,
			uploading: false,
		};
	}

	textInput: RefObject<HTMLInputElement>;
	photoInput: RefObject<HTMLInputElement>;

	reloadData = async () => {
		try {
			await userStore.getProfile();
			this.setState({
				firstName: userStore.user!.firstName,
				lastName: userStore.user!.lastName,
				notificationOn: userStore.user!.notificationOn,
				id: userStore.user!.id,
				profileImgUrl: userStore.user!.profileImgUrl ? userStore.user!.profileImgUrl : '/face.png',
				email: userStore.user!.email,
			});
		} catch (e) {
			console.error(e);
		}
	};
	onSave = async () => {
		await userStore.updateUser(this.state);
		await this.reloadData();
	};
	removeUser = async () => {
		if (window.confirm('Are you sure you wish to delete your account?')) {
			await userStore.removeUser(this.state.id);
			localStorage.clear();
			this.props.history.push('/welcome');
		}
	};

	onChange = (e: any) => {
		const files = Array.from(e.target.files);
		this.setState({ uploading: true });

		const formData = new FormData();

		formData.append('avatar', files[0] as any);

		const id = userStore.user!.id;

		userStore.uploadPhoto(id, formData).then(() => {
			this.setState({ uploading: false });
			this.reloadData();
		});
	};
	removePicture = () => {
		// TODO
	};

	render() {
		return (
			<div>
				<Nav projectsNav={true} onShare={() => ({})} peopleInProject={[]} history={this.props.history} />
				<main className="container">
					<div className="top">
						<Link to="/" className="btnset">
							<span className="btnset__box"></span>
							<span className="btnset__text">Projects</span>
						</Link>
						<div className="headline">Settings</div>
					</div>

					<div className="content">
						<div className="settings">
							<div className="form-headline form-headline--np">Avatar picture</div>
							<div className="avatar">
								<div className="avatar__left">
									<div className="avatar__img">
										<UserPhoto user={this.state} />
									</div>
								</div>
								<div className="avatar__right">
									<input type="file" id="single" ref={this.photoInput} hidden={true} onChange={this.onChange} />
									<button
										type="button"
										className="btn btn--upload"
										onClick={() => this.photoInput.current && this.photoInput.current.click()}>
										Upload picture
									</button>
									<button
										type="button"
										className="btn btn--blank"
										onClick={this.removePicture}>
										Remove picture
									</button>
									<br />
								</div>
							</div>

							<form onSubmit={e => e.preventDefault()}>
								<div className="form-headline">First Name</div>
								<div className="form-group">
									<input
										name="name"
										placeholder="First Name"
										type="text"
										className="form-input"
										onChange={e => this.setState({ firstName: e.target.value })}
										value={this.state.firstName}
									/>
								</div>
								<div>Last Name</div>
								<div className="form-group">
									<input
										name="name"
										placeholder="Last Name"
										type="text"
										className="form-input"
										onChange={e => this.setState({ lastName: e.target.value })}
										value={this.state.lastName}
									/>
								</div>
							</form>

							<div>
								<div className="form-headline">E-mail notifications</div>

								<div className="form-group">
									<div className="form-check">
										<label className="form-check-label">
											<input
												name="mailNotificationsType"
												id="standard"
												form=""
												type="radio"
												className="form-check-input"
												onChange={() => this.setState({ notificationOn: true })}
												checked={this.state.notificationOn}
											/>
											On
										</label>
									</div>
									<div className="form-text">
										You'll get an e-mail when someone @mentions you, assigns you to a task or project, or when someone comments a
										task you are subscribed to.
									</div>
								</div>

								<div className="form-group">
									<div className="form-check">
										<label className="form-check-label">
											<input
												name="mailNotificationsType"
												id="off"
												form=""
												type="radio"
												className="form-check-input"
												onChange={() => this.setState({ notificationOn: false })}
												checked={!this.state.notificationOn}
											/>
											Off
										</label>
									</div>
									<div className="form-text">You won't get any notification e-mails.</div>
								</div>

								<div className="form-group">
									<button type="submit" className="btn" onClick={() => this.onSave()}>
										Save changes
									</button>
									<button type="submit" className="btn btn--sec" onClick={() => this.reloadData()}>
										Cancel
									</button>
									<button className="btn btn--blank" onClick={() => this.removeUser()}>
										Delete this account permanently
									</button>
								</div>
							</div>
						</div>
					</div>
				</main>
			</div>
		);
	}
}

export default SettingsPage;
