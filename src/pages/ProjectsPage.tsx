import React, { Component, RefObject } from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import { observer } from 'mobx-react';
import { projectStore } from '../stores/ProjectsStore';
import Project from '../model/Project';
import ProjectRowComponent from '../components/ProjectRowComponent';
import Nav from '../components/Nav';
import * as queryString from 'querystring';
import { isArray } from 'util';
import { userStore } from '../stores/UsersStore';
import Utils from '../Utils';
import { DragDropContext, Droppable, Draggable, DropResult } from 'react-beautiful-dnd';

interface ProjectsPageState {
	projectName: string;
	openArchived: boolean;
	openNew: boolean;
	projects: Project[];
	loading: boolean;
}

@observer
class ProjectsPage extends Component<RouteComponentProps<any>, ProjectsPageState> {
	async componentDidMount(): Promise<void> {
		await this.reloadData();
	}

	constructor(props: RouteComponentProps<any>) {
		super(props);
		this.textInput = React.createRef();
		document.title = 'Gmanto - Projects';

		this.state = {
			projectName: '',
			openArchived: false,
			openNew: false,
			projects: [],
			loading: true,
		};
	}

	componentWillMount(): void {
		const query = queryString.parse(this.props.location.search);
		console.log('query', query);
		if (query['?token']) {
			localStorage.setItem('jwt', isArray(query['?token']) ? query['?token'][0] : query['?token']);
			this.props.history.push('/');
		}
	}

	textInput: RefObject<HTMLInputElement>;

	reloadData = async () => {
		try {
			this.setState({ loading: true });
			const projects = await projectStore.fetchAllProjects();
			this.setState({ projects, loading: false });
		} catch (e) {
			console.error(e);
		}
	};

	archiveProject = async (project: Project) => {
		project.archived = true;

		const projects = this.state.projects;
		const projectUpdatingIndex = projects.findIndex(p => p.id === project.id);
		projects[projectUpdatingIndex] = project;
		this.setState({ projects });

		await projectStore.editProject(project);
		await this.reloadData();
	};

	unarchiveProject = async (project: Project) => {
		project.archived = false;

		const projects = this.state.projects;
		const projectUpdatingIndex = projects.findIndex(p => p.id === project.id);
		projects[projectUpdatingIndex] = project;
		this.setState({ projects });

		await projectStore.editProject(project);
		await this.reloadData();
	};

	deleteProject = async (project: Project) => {
		if (window.confirm('Are you sure you wish to delete this project?')) {
			this.setState({ projects: this.state.projects.filter(p => p.id === project.id) });

			await projectStore.removeProject(project);
			await this.reloadData();
		}
	};

	_handleKeyDown = (e: any) => {
		if (e.key === 'Enter' && this.state.projectName !== '') {
			this.createProject();
		}
	};

	createProject = async () => {
		if (!userStore.user) {
			throw Error('user is not defined');
		}
		const projectToUser = { user: userStore.user!, owner: true, order: Utils.DEFAULT_ORDER, id: Utils.VOID_ID };
		const newProject = {
			name: this.state.projectName,
			archived: false,
			id: Utils.VOID_ID,
			taskLists: [],
			collaborators: [projectToUser],
			creator:  userStore.user!
		};
		this.setState({ projectName: '', openNew: false, projects: this.state.projects.concat([newProject]) });
		await projectStore.createProject(newProject);
		await this.reloadData();
	};

	dragEnd = async (dropResult: DropResult) => {
		console.log('dragResult', dropResult);
		if (dropResult.destination) {
			const sortedProjects = this.state.projects
				.filter(p => !p.archived)
				.sort(
					(a, b) =>
						a.collaborators.find(c => c.user.id === userStore.user!.id)!.order -
						b.collaborators.find(c => c.user.id === userStore.user!.id)!.order
				);
			const originalPosition = sortedProjects.findIndex(p => p.id === parseInt(dropResult.draggableId));
			const newlySorted = this.reorderProjectsToUser({ newIndex: dropResult.destination.index, oldIndex: originalPosition }, sortedProjects);
			this.setState({ projects: newlySorted });

			await projectStore.changePosition(parseInt(dropResult.draggableId), dropResult.destination.index);
			await this.reloadData();
		}
	};

	reorderProjectsToUser = (event: { newIndex: number; oldIndex: number }, originalArray: Project[]) => {
		const movedItem = originalArray.filter((item, index) => index === event.oldIndex);
		const remainingItems = originalArray.filter((item, index) => index !== event.oldIndex);

		const reorderedItems = [...remainingItems.slice(0, event.newIndex), movedItem[0], ...remainingItems.slice(event.newIndex)];

		return reorderedItems.map((c, i) => {
			c.collaborators.find(c => c.user.id === userStore.user!.id)!.order = i;
			return c;
		});
	};

	render() {
		const projectsNotArchived = this.state.projects
			.filter(p => !p.archived)
			.sort(
				(a, b) =>
					a.collaborators.find(c => c.user.id === userStore.user!.id)!.order -
					b.collaborators.find(c => c.user.id === userStore.user!.id)!.order
			);
		const projectsArchived = this.state.projects.filter(p => p.archived);
		return (
			<div>
				{this.state.loading && <div className="loading loading--active"></div>}
				<Nav projectsNav={true} onShare={() => ({})} peopleInProject={[]} history={this.props.history} />
				<main className="container">
					<div className="content">
						<div className="top">
							<div className="headline">Projects</div>
						</div>

						<div className="projects">
							<DragDropContext onDragEnd={this.dragEnd}>
								<Droppable droppableId={'projects'}>
									{(provided: any, snapshot: any) => (
										<div ref={provided.innerRef} {...provided.droppableProps}>
											{projectsNotArchived.map((project, index) => (
												<Draggable draggableId={project.id.toString()} index={index} key={project.id}>
													{(provided: any, snapshot: any) => (
														<Link
															to={'project-' + project.id}
															className="pbox"
															onClick={event => (project.id === Utils.VOID_ID ? event.preventDefault() : () => ({}))}
															{...provided.draggableProps}
															ref={provided.innerRef}
															{...provided.dragHandleProps}>
															<ProjectRowComponent
																project={project}
																archiveProject={this.archiveProject}
																unarchiveProject={this.unarchiveProject}
																deleteProject={this.deleteProject}
																reloadData={this.reloadData}
																{...this.props}
															/>
														</Link>
													)}
												</Draggable>
											))}
											{provided.placeholder}
										</div>
									)}
								</Droppable>
							</DragDropContext>

							{(this.state.projects.length === 0 || this.state.openNew) && (
								<div className="pbox">
									<div className="pbox__left">
										<form onSubmit={e => e.preventDefault()}>
											<input
												className="pbox__name"
												type="text"
												placeholder="Enter project name"
												value={this.state.projectName}
												ref={this.textInput}
												autoFocus={true}
												onKeyDown={this._handleKeyDown}
												onChange={event => this.setState({ projectName: event.target.value })}
											/>
										</form>
									</div>
								</div>
							)}
						</div>

						<div className="form-group">
							<button
								className={this.state.openNew ? 'btn btn--add btn--add-sec' : 'btn btn--add'}
								onClick={() => {
									this.setState({ openNew: !this.state.openNew, projectName: '' });
									if (this.textInput.current) {
										this.textInput.current.focus();
									}
								}}>
								Add new project
							</button>
							{this.state.projects.filter(p => p.archived).length !== 0 && (
								<button className="btn btn--blank" onClick={() => this.setState({ openArchived: !this.state.openArchived })}>
									{this.state.openArchived ? 'Hide archived projects' : 'Show archived projects'}
								</button>
							)}
						</div>

						{this.state.openArchived && (
							<div className="projects">
								{projectsArchived.map(project => (
									<Link to={'projects/' + project.id} className="pbox" key={project.id}>
										<ProjectRowComponent
											key={project.id}
											project={project}
											unarchiveProject={this.unarchiveProject}
											archiveProject={this.archiveProject}
											deleteProject={this.deleteProject}
											reloadData={this.reloadData}
											{...this.props}
										/>
									</Link>
								))}
							</div>
						)}
					</div>
				</main>
			</div>
		);
	}
}

export default ProjectsPage;
