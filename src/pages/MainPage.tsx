import React, { Component } from 'react';
import { RouteComponentProps } from 'react-router-dom';
import Configuration from '../Configuration';

class MainPage extends Component<RouteComponentProps<any>, {}> {
	render() {
		return (
			<main>
				<div className="loading loading--active"></div>

				<div className="toast">
					<div className="toast__box toast__box--fail toast__box--active">
						<div className="toast__close"></div>
						<div className="toast__text">Something went wrong, try it again later.</div>
					</div>
					<div className="toast__box toast__box--neutral toast__box--active">
						<div className="toast__close"></div>
						<div className="toast__text">It is going to be ok.</div>
					</div>
					<div className="toast__box toast__box--ok toast__box--active">
						<div className="toast__close"></div>
						<div className="toast__text">Gmanto is the best.</div>
					</div>
				</div>

				<div className="login">
					<div className="login__logo">
						<img src={process.env.PUBLIC_URL + '/logo.png'} alt="Gmanto" className="login__logo__img" />
						<div className="login__logo__text">Great management tool</div>
					</div>
					<div className="login__content">
						<div className="login__box">
							<div className="form-group">
								<a href={Configuration.API_URL + '/auth/google'} className="btn btn--wide">
									Log in with Google
								</a>
							</div>
							<small>
								You are running this application in <b>{process.env.NODE_ENV}</b> mode.
							</small>
						</div>
						<div className="login__box-down">
							<button className="btn btn--blank">Create new Google account</button>
						</div>
					</div>
				</div>
			</main>
		);
	}
}

export default MainPage;
