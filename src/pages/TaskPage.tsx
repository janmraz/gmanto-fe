import { observer } from 'mobx-react';
import { Component, RefObject } from 'react';
import { Link, RouteComponentProps } from 'react-router-dom';
import React from 'react';
import { tasksStore } from '../stores/TasksStore';
import Task from '../model/Task';
import { taskListStore } from '../stores/TaskListStore';
import DatePicker from 'react-datepicker';
import DropdownUsersComponent from '../components/DropdownUsersComponent';
import User from '../model/User';
import { projectStore } from '../stores/ProjectsStore';
import { MergeTags, TrixEditor } from 'react-trix';
import 'trix/dist/trix';
import 'trix/dist/trix.css';
import { commentsStore } from '../stores/CommentsStore';
import { userStore } from '../stores/UsersStore';
import Nav from '../components/Nav';
import CommentLineComponent from '../components/CommentLineComponent';
import NotificationComponent from '../components/NotificationsComponent';
import Utils from '../Utils';
import socketIOClient from 'socket.io-client';
import { SocketDTO } from '../model/SocketDTO';
import Configuration from '../Configuration';
import DropdownFollowersComponent from '../components/DropdownFollowersComponent';
import ModalComponent from '../components/ModalComponent';
import UserPhoto from '../components/UserPhotoComponent';
import Comment from '../model/Comment';
import ProjectToUser from '../model/ProjectToUser';
const { DateTime } = require('luxon');

interface TaskPageState {
	showCommentsOnly: boolean;
	task: Task | null;
	peopleInProject: ProjectToUser[];
	projectOwner: User | null;
	projectId: number;
	taskListName: string;
	taskListId: number;
	renameField: string;
	renameOpen: boolean;
	optionsOpen: boolean;
	openModal: boolean;
	editor: any | null;
	mergeTags: MergeTags[];
	comment: string;
	notifications: Comment[];
	currentNotificationsSkip: number;
	loading: boolean;
}

@observer
class TaskPage extends Component<RouteComponentProps<any>, TaskPageState> {
	constructor(props: any) {
		super(props);
		this.textInput = React.createRef();
		this.options = React.createRef();
		this.state = {
			showCommentsOnly: false,
			task: null,
			taskListName: '',
			taskListId: Utils.VOID_ID,
			renameField: '',
			renameOpen: false,
			optionsOpen: false,
			openModal: false,
			peopleInProject: [],
			projectOwner: null,
			projectId: Utils.VOID_ID,
			editor: null,
			mergeTags: [],
			comment: '',
			notifications: [],
			currentNotificationsSkip: 0,
			loading: true
		};
	}

	textInput: RefObject<HTMLInputElement>;
	options: RefObject<HTMLDivElement>;

	componentWillMount(): void {
		document.addEventListener('mousedown', this._handleClick, false);
	}
	componentWillUnmount(): void {
		document.removeEventListener('mousedown', this._handleClick, false);
	}

	_handleClick = (e: any) => {
		if (this.options.current && this.options.current.contains(e.target)) {
			return;
		}

		this.setState({ optionsOpen: false });
	};

	async componentDidMount() {
		const socket = socketIOClient(Configuration.API_URL);
		socket.on('data', this.receivedDTO);
		await this.reloadData();
	}

	receivedDTO = async (dto: SocketDTO) => {
		console.log('received socket', dto);
		if (dto.taskId === parseInt(this.props.match.params.taskId)) {
			this.reloadData();
		}
	};

	setTitle = (nots?: number, taskName?: string) => {
		if (nots) {
			document.title = taskName ? '(' + nots + ') Gmanto - ' + taskName : '(' + nots + ') Gmanto';
		} else {
			document.title = taskName ? 'Gmanto - ' + taskName : 'Gmanto';
		}
	};

	reloadData = async () => {
		try {
			this.setState({loading: true});
			await Promise.all([
				projectStore.fetchProject(this.props.match.params.id).then(project => {
					const mergeTags = [
						{
							trigger: '@',
							tags: this.transformPeopleIntoTags(project.collaborators.map(ptu => ptu.user)),
						},
					];
					this.setState({
						peopleInProject: project.collaborators,
						projectId: project.id,
						mergeTags,
					});
				}),
				// taskListId is not used in BE !!!
				tasksStore.fetchTask(Utils.VOID_ID, parseInt(this.props.match.params.taskId)).then(async task => {
					// @ts-ignore
					this.setState({ task, renameField: task.name, taskListName: task && task.taskList ? task.taskList.name : '' });
					if (task.comments.filter(c => !c.seen).length > 0) {
						await commentsStore.setSeen(
							parseInt(this.props.match.params.id),
							task.comments.filter(c => !c.seen)
						);
					}
				}),
				commentsStore.getCommentInfo(parseInt(this.props.match.params.id), 0, Utils.NOTIFICATIONS_BATCH).then(receivedNotifications => {
					const newNotifications = this.state.notifications;
					receivedNotifications.forEach(c => {
						if (newNotifications.findIndex(n => n.id === c.id) < 0) {
							newNotifications.push(c);
						}
					});
					this.setState({ notifications: receivedNotifications });
				}),
			]);

			// notifications title update
			let countNotifications = 0;
			taskListStore.taskLists.forEach(tsklt =>
				tsklt.tasks.forEach(tsk =>
					tsk.comments.forEach(c => {
						if (!c.seen) countNotifications++;
					})
				)
			);
			this.setTitle(countNotifications, this.state.task?.name);
			this.setState({loading: false});
		} catch (e) {
			console.error(e);
			if (e && e.response && e.response.status === 401) {
				window.location.href = '/projects/' + this.props.match.params.id + '/access-denied';
			}
		}
	};

	fetchMoreNotifications = async () => {
		const receivedNotifications = await commentsStore.getCommentInfo(
			parseInt(this.props.match.params.id),
			this.state.currentNotificationsSkip,
			Utils.NOTIFICATIONS_BATCH
		);
		const newNotifications = this.state.notifications;
		receivedNotifications.forEach(c => {
			if (newNotifications.findIndex(n => n.id === c.id) < 0) {
				newNotifications.push(c);
			}
		});
		this.setState({ notifications: newNotifications });
	};

	transformPeopleIntoTags = (users: User[]) => {
		return users.map(user => ({ name: user.firstName + ' ' + user.lastName, tag: '@' + user.firstName + '.' + user.lastName + '.' + user.id }));
	};

	toggleTask = async () => {
		if (this.state.task && !this.state.task.done) {
			await tasksStore.doneTask(this.state.task, this.state.taskListId);
		} else if (this.state.task) {
			await tasksStore.undoneTask(this.state.task, this.state.taskListId);
		}
		await this.reloadData();
		this.forceUpdate();
	};

	_handleKeyDown = (e: any) => {
		if (e.key === 'Enter' && this.state.renameField !== '' && this.state.task) {
			const task = this.state.task;
			task.name = this.state.renameField;
			this.setState({ task });

			tasksStore.renameTask(this.state.taskListId, this.state.task.id, this.state.renameField).then(() => {
				this.setState({ renameOpen: false });
				this.reloadData();
			});
		}
	};

	rename = () => {
		this.setState({ renameOpen: true });
		if (this.textInput.current) {
			this.textInput.current.focus();
		}
	};

	changedDeadline = async (date: Date | null) => {
		if (this.state.task) {
			if(date && Utils.isToday(date!)){
				date = null;
			}
			const task = this.state.task;
			task.dateDue = date;
			this.setState({ task });

			await tasksStore.changeDeadline(task, this.state.taskListId);
			await this.reloadData();
		}
	};

	delete = async () => {
		if (window.confirm('Are you sure you wish to delete this task?') && this.state.task) {
			await tasksStore.removeTask(this.state.taskListId, this.state.task.id);
			this.props.history.push('/project-' + this.props.match.params.id);
		}
	};

	inviteCallback = async () => {
		this.setState({ openModal: true });
	};

	changedAssignee = async (user: User | null) => {
		if (this.state.task) {
			const task = this.state.task;
			task.assignee = user;
			this.setState({ task });

			await tasksStore.changeAssignee(task, this.state.taskListId);
			await this.reloadData();
		}
	};

	addFollower = async (user: User) => {
		if (this.state.task) {
			const task = this.state.task;
			task.followers.push(user);
			this.setState({ task });

			await tasksStore.addFollower(this.state.task, this.state.taskListId, user);
			await this.reloadData();
		}
	};

	removeFollower = async (user: User) => {
		if (this.state.task) {
			const task = this.state.task;
			task.followers = task.followers.filter(f => f.id === user.id);
			this.setState({ task });

			await tasksStore.removeFollower(this.state.task, this.state.taskListId, user);
			await this.reloadData();
		}
	};

	handleEditorReady = (editor: any) => {
		this.setState({ editor });
	};

	commentChanged = (html: string, text: string) => {
		if (this.state) {
			this.setState({ comment: html });
		}
	};

	send = async () => {
		if (this.state.task) {
			const task = this.state.task;
			const comment = {
				id: Utils.VOID_ID,
				type: '',
				text: this.state.comment,
				creator: userStore.user!,
				dateCreated: new Date(),
				dateModified: new Date(),
				infoComment: false,
				seen: false,
			};
			task.comments.unshift(comment);
			this.setState({ task });

			await commentsStore.createComment(comment, this.state.task.id);
			if (this.state.editor) {
				this.state.editor.loadHTML('');
			}
			this.setState({ comment: '' });
			await this.reloadData();
		}
	};
	editComment = async (comment: string, commentId: number) => {
		if (this.state.task) {
			const task = this.state.task;
			task.comments = task.comments.map(c => {
				if (c.id === commentId) {
					c.text = comment;
				}
				return c;
			});
			this.setState({ task });

			await commentsStore.editComment(
				{
					id: commentId,
					type: '',
					text: comment,
					creator: userStore.user!,
					dateCreated: new Date(),
					dateModified: new Date(),
					infoComment: false,
					seen: false,
				},
				this.state.task.id
			);
			await this.reloadData();
		}
	};

	deleteComment = async (commentId: number) => {
		if (window.confirm('Are you sure you wish to delete this comment?') && this.state.task) {
			const task = this.state.task;
			task.comments = task.comments.filter(c => c.id !== commentId);
			this.setState({ task });

			await commentsStore.removeComment(commentId, this.state.task.id);
			await this.reloadData();
		}
	};

	invite = async (email: string) => {
		await projectStore.addCollaborator(this.state.projectId, email);
		await this.reloadData();
	};

	changeOwnership = async (user: User) => {
		this.setState({
			peopleInProject: this.state.peopleInProject.map(p => {
				if (p.owner) {
					p.owner = false;
				}
				if (p.user.id === user.id) {
					p.owner = true;
				}
				return p;
			}),
		});

		await projectStore.changeOwner(this.state.projectId, user);
		await this.reloadData();
	};

	removeUser = async (user: User) => {
		if (window.confirm('Are you sure you wish to remove this collaborator?')) {
			this.setState({ peopleInProject: this.state.peopleInProject.filter(p => p.user.id !== user.id) });

			await projectStore.removeCollaborator(this.state.projectId, user);
			await this.reloadData();
		}
	};

	onOpen = async () => {
		this.setState({
			notifications: this.state.notifications.map(n => {
				n.seen = true;
				return n;
			}),
		});

		await commentsStore.setSeen(this.state.projectId, this.state.notifications);
		await this.reloadData();
	};

	onClickedMore = async () => {
		await this.setState({ currentNotificationsSkip: this.state.currentNotificationsSkip + Utils.NOTIFICATIONS_BATCH });
		await this.fetchMoreNotifications();
	};

	render() {
		const taskListId = this.state.taskListId;
		const taskId = this.state.task ? this.state.task.id : Utils.VOID_ID;
		return (
			<div className="container">
				<Nav
					projectsNav={false}
					onShare={() => this.setState({ openModal: true })}
					peopleInProject={this.state.peopleInProject}
					history={this.props.history}
				/>
				<NotificationComponent
					projectId={parseInt(this.props.match.params.id)}
					notifications={this.state.notifications}
					onClickMore={this.onClickedMore}
					onOpen={this.onOpen}
				/>
				{this.state.openModal && (
					<ModalComponent
						open={this.state.openModal}
						onClose={() => this.setState({ openModal: false })}
						invite={this.invite}
						peopleInProject={this.state.peopleInProject}
						projectOwner={this.state.projectOwner}
						removeUser={this.removeUser}
						makeOwner={this.changeOwnership}
					/>
				)}

				<div className="top">
					<Link to={'/project-' + this.props.match.params.id} className="btnset">
						<span className="btnset__box"></span>
						<span className="btnset__text">Tasks</span>
					</Link>
					<div className="headline">{this.state.taskListName}</div>
				</div>

				<div className="content">
					<div className="detail">
						<div className="detail__top">
							<div className="detail__top__left">
								<form>
									<label htmlFor="task" className="task__checkbox task__checkbox--big" onClick={this.toggleTask}>
										<input name="task" type="checkbox" className="h-hidden" />
										<div
											className={
												this.state.task && this.state.task.done
													? 'task__checkbox__img task__checkbox__img--done'
													: 'task__checkbox__img'
											}></div>
									</label>
								</form>
								<div className="task__name task__name--big">
									<input
										type="text"
										name="task name"
										className="task__name__input"
										value={this.state.renameField}
										ref={this.textInput}
										autoFocus={true}
										placeholder="Enter task name"
										onKeyDown={this._handleKeyDown}
										onChange={e => this.setState({ renameField: e.target.value })}
									/>
								</div>
							</div>
							<div
								ref={this.options}
								className="detail__top__right"
								onClick={() => this.setState({ optionsOpen: !this.state.optionsOpen })}>
								<div className="detail__top__icons">
									<div className={this.state.optionsOpen ? 'dropdown dropdown--active' : 'dropdown'}>
										<div className="icon icon--more"></div>
										<div className="dropdown__menu">
											<div className="dropdown__content">
												<div className="dropdown__set">
													<div className="dropdown__option" onClick={this.rename}>
														Rename
													</div>
													<div className="dropdown__option" onClick={this.delete}>
														Delete
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div className="detail__bottom">
							<div className="detail__bottom__left">
								<div className="task__infotext">Assigned to:</div>
								{this.state.task && (
									<DropdownUsersComponent
										task={this.state.task}
										inviteUser={this.inviteCallback}
										peopleInProject={this.state.peopleInProject}
										onChangeUser={this.changedAssignee}
									/>
								)}
								<div className="task__date">
									<DatePicker
										selected={this.state.task && this.state.task.dateDue ? new Date(this.state.task.dateDue) : null}
										onChange={this.changedDeadline}
										todayButton={'No due date'}
										placeholderText="Set Deadline"
									/>
								</div>
							</div>
							<div className="detail__bottom__right">
								<div className="task__infotext">Followers:</div>
								{this.state.task && (
									<DropdownFollowersComponent
										task={this.state.task}
										inviteUser={this.inviteCallback}
										peopleInProject={this.state.peopleInProject}
										addUser={this.addFollower}
										removeUser={this.removeFollower}
									/>
								)}
							</div>
						</div>

						<div className="detail__comments">
							<div className="comment comment--new">
								<div className="comment__left">
									<div className="circles">
										<div className="circle">{userStore.user && <UserPhoto user={userStore.user} />}</div>
									</div>
								</div>
								<div className="comment__right">
									<div className="texta">
										<TrixEditor
											autoFocus={true}
											placeholder="Write a comment or upload a file"
											onChange={(html: string, text: string) => this.commentChanged(html, text)}
											onEditorReady={this.handleEditorReady}
											value={this.state.comment}
											mergeTags={this.state.mergeTags}
											uploadURL={Configuration.API_URL + '/taskLists/' + taskListId + '/tasks/' + taskId + '/upload-file'}
										/>

										<div className="texta__control">
											{/*<div className="texta__control__left">*/}
											{/*	<div className="texta__icon texta__icon--b"></div>*/}
											{/*	<div className="texta__icon texta__icon--i"></div>*/}
											{/*	<div className="texta__icon texta__icon--u"></div>*/}
											{/*	<div className="texta__icon texta__icon--ul"></div>*/}
											{/*	<div className="texta__icon texta__icon--ol"></div>*/}
											{/*</div>*/}
											<div className="texta__control__right">
												{/*<div className="texta__icon texta__icon--file"></div>*/}
												<button className="btn" onClick={this.send}>
													Send
												</button>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div
								className={this.state.showCommentsOnly ? 'switch switch--on' : 'switch'}
								onClick={() => this.setState({ showCommentsOnly: !this.state.showCommentsOnly })}>
								<div className="switch__content">
									<div className="switch__text">Show comments only</div>
									<div className="switch__box">
										<div className="switch__box__in"></div>
									</div>
								</div>
							</div>

							{!this.state.showCommentsOnly &&
								this.state.task &&
								this.state.task.comments
									.sort((a, b) => DateTime.fromISO(b.dateCreated).valueOf() - DateTime.fromISO(a.dateCreated).valueOf())
									.map(comment => (
										<CommentLineComponent
											key={comment.id}
											comment={comment}
											deleteComment={this.deleteComment}
											taskListId={this.state.taskListId}
											taskId={this.state.task ? this.state.task.id : Utils.VOID_ID}
											mergeTags={this.state.mergeTags}
											editComment={this.editComment}
										/>
									))}
							{this.state.showCommentsOnly &&
								this.state.task &&
								this.state.task.comments
									.filter(com => !com.infoComment)
									.sort((a, b) => DateTime.fromISO(b.dateCreated).valueOf() - DateTime.fromISO(a.dateCreated).valueOf())
									.map(comment => (
										<CommentLineComponent
											key={comment.id}
											comment={comment}
											deleteComment={this.deleteComment}
											taskListId={this.state.taskListId}
											taskId={this.state.task ? this.state.task.id : Utils.VOID_ID}
											mergeTags={this.state.mergeTags}
											editComment={this.editComment}
										/>
									))}
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default TaskPage;
