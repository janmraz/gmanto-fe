import React, { Component } from 'react';
import { RouteComponentProps } from 'react-router-dom';
import { userStore } from '../stores/UsersStore';
import { projectStore } from '../stores/ProjectsStore';
import Nav from '../components/Nav';

class AccessDeniedPage extends Component<RouteComponentProps<any>, {}> {
	request = async () => {
		const currentUser = userStore.user!;
		const projectId = this.props.match.params.id;
		await projectStore.requestAccess(projectId, currentUser);
	};

	render() {
		return (
			<main>
				<Nav projectsNav={true} peopleInProject={[]} onShare={() => ({})} history={this.props.history} />
				<h1> You have no permission to see this page!</h1>
				<div>
					<button onClick={this.request}>Request Acccess!</button>
				</div>
			</main>
		);
	}
}

export default AccessDeniedPage;
