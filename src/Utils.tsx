export default class Utils {
	public static VOID_ID: number = -1;
	public static DEFAULT_ORDER: number = 10000;
	public static NOTIFICATIONS_BATCH: number = 8;
	public static DONE_TASK_BATCH: number = 2;

	static prepareHeaders(): object {
		const jwt = localStorage.getItem('jwt');
		return { authorization: 'Bearer ' + jwt };
	}
	static prepareFormHeaders(): object {
		const jwt = localStorage.getItem('jwt');
		return { authorization: 'Bearer ' + jwt, contentType: 'multipart/form-data' };
	}
	static stripPrefixFromString(text: string): string {
		return text.split('-')[1];
	}
	static isToday(someDate: Date): boolean {
		const today = new Date();
		return someDate.getDate() === today.getDate() && someDate.getMonth() === today.getMonth() && someDate.getFullYear() === today.getFullYear();
	}
	static isDateBeforeToday(date: Date): boolean {
		return new Date(date.toDateString()) < new Date(new Date().toDateString());
	}
}
