import { action } from 'mobx';
import axios from 'axios';
import Configuration from '../Configuration';
import User from '../model/User';
import Utils from '../Utils';

class UsersStore {
	public user: User | undefined;

	@action
	async logout() {
		await axios.get(Configuration.API_URL + '/users/logout', { headers: Utils.prepareHeaders() });
	}

	@action
	async getProfile() {
		const response = await axios.get(Configuration.API_URL + '/users/profile', { headers: Utils.prepareHeaders() });
		this.user = response.data;
	}

	@action
	async updateUser(user: User) {
		const response = await axios.post(Configuration.API_URL + '/users/user/' + user.id, user, { headers: Utils.prepareHeaders() });
		this.user = response.data;
	}
	@action
	async removeUser(userId: number) {
		const response = await axios.delete(Configuration.API_URL + '/users/user/' + userId, { headers: Utils.prepareHeaders() });
		this.user = response.data;
	}

	@action
	async uploadPhoto(userId: number, formData: FormData) {
		await axios.post(Configuration.API_URL + '/users/user/' + userId + '/upload-picture', formData, { headers: Utils.prepareFormHeaders() });
	}
}

export const userStore = new UsersStore();
