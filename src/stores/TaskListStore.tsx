import { action, observable } from 'mobx';
import axios from 'axios';
import Configuration from '../Configuration';
import { isArray } from 'util';
import TaskList from '../model/TaskList';
import Utils from '../Utils';

class TaskListStore {
	@observable taskLists: TaskList[] = [];

	private RESOURCE_URL: (projectId: number) => string = (projectId: number) => '/projects/' + projectId + '/taskLists';

	@action
	async fetchAllTaskLists(projectId: number) {
		const response = await axios.get(Configuration.API_URL + this.RESOURCE_URL(projectId), { headers: Utils.prepareHeaders() });
		if (!isArray(response.data)) {
			throw new Error('not an array');
		}
		this.taskLists = response.data;
	}

	@action
	async fetchTaskList(projectId: number, id: number): Promise<TaskList> {
		return axios
			.get(Configuration.API_URL + this.RESOURCE_URL(projectId) + '/' + id, { headers: Utils.prepareHeaders() })
			.then(response => response.data);
	}

	@action
	async createTaskList(projectId: number, taskList: TaskList) {
		await axios.post(Configuration.API_URL + this.RESOURCE_URL(projectId), taskList, { headers: Utils.prepareHeaders() });
	}

	@action
	async changeOrderTaskList(projectId: number, taskListId: number, order: number) {
		await axios.post(
			Configuration.API_URL + this.RESOURCE_URL(projectId) + '/' + taskListId + '/change-order',
			{ order },
			{ headers: Utils.prepareHeaders() }
		);
	}

	@action
	async removeTaskList(projectId: number, taskList: TaskList) {
		await axios.delete(Configuration.API_URL + this.RESOURCE_URL(projectId) + '/' + taskList.id, { headers: Utils.prepareHeaders() });
	}

	@action
	async editTaskList(projectId: number, taskList: TaskList) {
		await axios.put(Configuration.API_URL + this.RESOURCE_URL(projectId) + '/' + taskList.id, taskList, { headers: Utils.prepareHeaders() });
	}
}

export const taskListStore = new TaskListStore();
