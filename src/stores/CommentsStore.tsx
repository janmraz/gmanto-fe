import { action } from 'mobx';
import axios from 'axios';
import Configuration from '../Configuration';
import Utils from '../Utils';
import Comment from '../model/Comment';

class CommentsStore {
	private RESOURCE_URL = (taskId: number) => '/tasks/' + taskId + '/comments';

	@action
	async createComment(comment: Comment, taskId: number): Promise<Comment> {
		return axios
			.post(Configuration.API_URL + this.RESOURCE_URL(taskId) + '/', comment, { headers: Utils.prepareHeaders() })
			.then(response => response.data);
	}

	@action
	async editComment(comment: Comment, taskId: number): Promise<Comment> {
		return axios
			.put(Configuration.API_URL + this.RESOURCE_URL(taskId) + '/' + comment.id, comment, { headers: Utils.prepareHeaders() })
			.then(response => response.data);
	}

	@action
	async removeComment(commentId: number, taskId: number): Promise<Comment> {
		return axios
			.delete(Configuration.API_URL + this.RESOURCE_URL(taskId) + '/' + commentId, { headers: Utils.prepareHeaders() })
			.then(response => response.data);
	}

	@action
	async getCommentInfo(projectId: number, skip: number, take: number): Promise<Comment[]> {
		return axios
			.get(Configuration.API_URL + '/projects/' + projectId + '/notifications', { params: { skip, take},headers: Utils.prepareHeaders() })
			.then(response => response.data);
	}

	@action
	async setSeen(projectId: number, comments: Comment[]): Promise<void> {
		await axios
			.post(Configuration.API_URL + '/projects/' + projectId + '/notifications', { commentIds: comments.map(c => c.id) }, { headers: Utils.prepareHeaders() })
			.then(response => response.data);
	}
}

export const commentsStore = new CommentsStore();
