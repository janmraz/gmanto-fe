import { action, observable } from 'mobx';
import axios from 'axios';
import Configuration from '../Configuration';
import { isArray } from 'util';
import Project from '../model/Project';
import Utils from '../Utils';
import User from "../model/User";

class ProjectStore {
	@observable projects: Project[] = [];

	private RESOURCE_URL: string = '/projects';

	@action
	async fetchAllProjects() {
		const response = await axios.get(Configuration.API_URL + this.RESOURCE_URL, { headers: Utils.prepareHeaders() });
		if (!isArray(response.data)) {
			throw new Error('not an array');
		}
		this.projects = response.data;
		return response.data;
	}

	@action
	async fetchProject(id: number): Promise<Project> {
		return axios.get(Configuration.API_URL + this.RESOURCE_URL + '/' + id, { headers: Utils.prepareHeaders() }).then(response => response.data);
	}

	@action
	async createProject(project: Project) {
		await axios.post(Configuration.API_URL + this.RESOURCE_URL, project, { headers: Utils.prepareHeaders() });
	}

	@action
	async removeProject(project: Project) {
		await axios.delete(Configuration.API_URL + this.RESOURCE_URL + '/' + project.id, { headers: Utils.prepareHeaders() });
	}

	@action
	async editProject(project: Project) {
		await axios.put(Configuration.API_URL + this.RESOURCE_URL + '/' + project.id, project, { headers: Utils.prepareHeaders() });
	}

	@action
	async addCollaborator(projectId: number, email: string) {
		await axios.post(Configuration.API_URL + this.RESOURCE_URL + '/' + projectId + '/invite', { email }, { headers: Utils.prepareHeaders() });
	}
	@action
	async removeCollaborator(projectId: number, user: User) {
		await axios.post(Configuration.API_URL + this.RESOURCE_URL + '/' + projectId + '/remove-collaborator', { user }, { headers: Utils.prepareHeaders() });
	}
	@action
	async changeOwner(projectId: number, owner: User) {
		await axios.post(Configuration.API_URL + this.RESOURCE_URL + '/' + projectId + '/change-owner', { user: owner }, { headers: Utils.prepareHeaders() });
	}
	@action
	async changePosition(projectId: number, order: number) {
		await axios.post(Configuration.API_URL + this.RESOURCE_URL + '/' + projectId + '/change-order', { order }, { headers: Utils.prepareHeaders() });
	}
	@action
	async requestAccess(projectId: number, requestingUser: User) {
		await axios.post(Configuration.API_URL + this.RESOURCE_URL + '/' + projectId + '/request-access', { user: requestingUser }, { headers: Utils.prepareHeaders() });
	}
}

export const projectStore = new ProjectStore();
