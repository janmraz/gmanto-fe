import { action } from 'mobx';
import axios from 'axios';
import Configuration from '../Configuration';
import Task from '../model/Task';
import Utils from '../Utils';
import User from '../model/User';

class TasksStore {
	private RESOURCE_URL: (taskListId: number) => string = (taskListId: number) => '/taskLists/' + taskListId + '/tasks';

	@action
	async fetchTask(taskListId: number, id: number): Promise<Task> {
		return axios
			.get(Configuration.API_URL + this.RESOURCE_URL(taskListId) + '/' + id, { headers: Utils.prepareHeaders() })
			.then(response => response.data);
	}

	@action
	async createTask(taskListId: number, task: Task) {
		await axios.post(Configuration.API_URL + this.RESOURCE_URL(taskListId), task, { headers: Utils.prepareHeaders() });
	}

	@action
	async renameTask(taskListId: number, taskId: number, name: string) {
		await axios.post(
			Configuration.API_URL + this.RESOURCE_URL(taskListId) + '/' + taskId + '/rename',
			{ name },
			{ headers: Utils.prepareHeaders() }
		);
	}

	@action
	async doneTask(task: Task, taskListId: number) {
		await axios.post(Configuration.API_URL + this.RESOURCE_URL(taskListId) + '/' + task.id + '/done', task, { headers: Utils.prepareHeaders() });
	}

	@action
	async changeDeadline(task: Task, taskListId: number) {
		await axios.post(
			Configuration.API_URL + this.RESOURCE_URL(taskListId) + '/' + task.id + '/change-deadline',
			{ deadline: task.dateDue },
			{ headers: Utils.prepareHeaders() }
		);
	}

	@action
	async addFollower(task: Task, taskListId: number, user: User) {
		await axios.post(
			Configuration.API_URL + this.RESOURCE_URL(taskListId) + '/' + task.id + '/add-follower',
			{ user },
			{ headers: Utils.prepareHeaders() }
		);
	}

	@action
	async removeFollower(task: Task, taskListId: number, user: User) {
		await axios.post(
			Configuration.API_URL + this.RESOURCE_URL(taskListId) + '/' + task.id + '/remove-follower',
			{ user },
			{ headers: Utils.prepareHeaders() }
		);
	}

	@action
	async changeAssignee(task: Task, taskListId: number) {
		await axios.post(
			Configuration.API_URL + this.RESOURCE_URL(taskListId) + '/' + task.id + '/change-assignee',
			{ assignee: task.assignee },
			{ headers: Utils.prepareHeaders() }
		);
	}

	@action
	async changeOrder(taskListId: number, taskId: number, order: number) {
		await axios.post(
			Configuration.API_URL + this.RESOURCE_URL(taskListId) + '/' + taskId + '/change-order',
			{ order },
			{ headers: Utils.prepareHeaders() }
		);
	}

	@action
	async moveTask(taskListId: number, taskId: number, order: number) {
		await axios.post(
			Configuration.API_URL + this.RESOURCE_URL(taskListId) + '/' + taskId + '/move-task',
			{ order },
			{ headers: Utils.prepareHeaders() }
		);
	}

	@action
	async undoneTask(task: Task, taskListId: number) {
		await axios.post(Configuration.API_URL + this.RESOURCE_URL(taskListId) + '/' + task.id + '/undone', task, {
			headers: Utils.prepareHeaders(),
		});
	}

	@action
	async removeTask(taskListId: number, taskId: number) {
		await axios.delete(Configuration.API_URL + this.RESOURCE_URL(taskListId) + '/' + taskId, { headers: Utils.prepareHeaders() });
	}
}

export const tasksStore = new TasksStore();
