import React from 'react';
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';
import MainPage from './pages/MainPage';
import { BrowserRouter } from 'react-router-dom';
import ProjectsPage from './pages/ProjectsPage';
import TaskListPage from './pages/TaskListPage';
import TaskPage from './pages/TaskPage';

import './style.css';
import 'react-datepicker/dist/react-datepicker.css';
import SettingsPage from './pages/SettingsPage';
import AccessDeniedPage from './pages/AccessDeniedPage';

const App: React.FC = () => {
	const routes = (
		<Router>
			<Switch>
				<Route exact path="/welcome" component={MainPage} />
				<Route exact path="/" component={ProjectsPage} />
				<Route exact path="/settings" component={SettingsPage} />
				<Route exact path="/project-:id" component={TaskListPage} />
				<Route exact path="/project-:id/access-denied" component={AccessDeniedPage} />
				<Redirect from="/project-:id/task-:taskId/reload" to="/project-:id/task-:taskId" />
				<Route exact path="/project-:id/task-:taskId" component={TaskPage} />
			</Switch>
		</Router>
	);

	return <BrowserRouter children={routes} />;
};

export default App;
