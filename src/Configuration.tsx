export default class Configuration {
	public static API_URL: string = process.env.NODE_ENV === "development" ? "http://localhost:5000" :'https://gmanto-be.herokuapp.com';
}
