export default interface Notification {
	text: string;
	created: Date;
}
