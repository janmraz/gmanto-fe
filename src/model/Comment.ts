import User from './User';

export default interface Comment {
	id: number;
	text: string;
	type: string;
	creator: User;
	dateCreated: Date;
	dateModified: Date;
	infoComment: boolean;
	seen: boolean | undefined;
}