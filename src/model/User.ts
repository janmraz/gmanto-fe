export default interface User {
    id: number;
    firstName: string;
    lastName: string;
    email: string;
    profileImgUrl: string;
    notificationOn: boolean;
    darkMode: boolean;
}