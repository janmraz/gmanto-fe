import User from './User';
import Comment from './Comment';

export default interface Task {
	id: number;
	name: string;
	done: boolean;
	dateDue: Date | null;
	assignee: User | null;
	comments: Comment[];
	followers: User[];
	order: number;
}
