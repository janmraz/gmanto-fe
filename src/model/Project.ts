import TaskList from './TaskList';
import User from "./User";
import ProjectToUser from "./ProjectToUser";

export default interface Project {
	id: number;
	name: string;
	archived: boolean;
	tasksOpen?: number;
	tasksMine?: number;
	collaborators: ProjectToUser[];
	taskLists: TaskList[];
}
