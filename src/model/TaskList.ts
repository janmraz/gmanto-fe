import Task from './Task';

export default interface TaskList {
	id: number;
	name: string;
	tasks: Task[];
	archived: boolean;
	order: number
}
