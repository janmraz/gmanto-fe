import Project from "./Project";
import User from "./User";

export default interface ProjectToUser {
    id: number;
    user: User;
    order: number;
    owner: boolean;
}