import React, { Component, RefObject } from 'react';
import TaskList from '../model/TaskList';
import { RouteComponentProps } from 'react-router-dom';
import TaskComponent from './TaskComponent';
import Task from '../model/Task';
import User from '../model/User';
import { Draggable, Droppable } from 'react-beautiful-dnd';
import ProjectToUser from '../model/ProjectToUser';
import Utils from '../Utils';

interface TaskListProps extends RouteComponentProps<any> {
	taskList: TaskList;
	peopleInProject: ProjectToUser[];
	index: number;
	createTask: (name: string, taskListId: number) => void;
	updateTaskList: (taskList: TaskList) => void;
	deleteTaskList: (taskList: TaskList) => void;
	toggledTask: (task: Task, taskList: number, value: boolean) => void;
	changedDeadline: (task: Task, taskList: TaskList, date: Date | null) => void;
	changedAssignee: (task: Task, taskList: TaskList, user: User | null) => void;
	inviteUser: () => void;
}

interface TaskListComponentState {
	taskName: string;
	taskListName: string;
	openSideMenu: boolean;
	openNew: boolean;
	renameMode: boolean;
	openCompleted: boolean;
	allowedDoneTasks: number;
}

class TaskListComponent extends Component<TaskListProps, TaskListComponentState> {
	constructor(props: TaskListProps) {
		super(props);
		this.textInputTaskListRename = React.createRef();
		this.textInputTaskNew = React.createRef();
		this.options = React.createRef();
		this.state = {
			taskName: '',
			taskListName: this.props.taskList.name,
			openSideMenu: false,
			openNew: false,
			renameMode: false,
			openCompleted: false,
			allowedDoneTasks: Utils.DONE_TASK_BATCH,
		};
	}
	textInputTaskListRename: RefObject<HTMLInputElement>;
	textInputTaskNew: RefObject<HTMLInputElement>;

	options: RefObject<HTMLDivElement>;

	componentWillMount(): void {
		document.addEventListener('mousedown', this._handleClick, false);
	}
	componentWillUnmount(): void {
		document.removeEventListener('mousedown', this._handleClick, false);
	}

	_handleClick = (e: any) => {
		if (this.options.current && this.options.current.contains(e.target)) {
			return;
		}

		this.setState({ openSideMenu: false });
	};

	_handleKeyDownTaskListRename = (e: any) => {
		if (e.key === 'Enter') {
			this.updateTaskList();
		}
	};
	_handleKeyDownTaskNew = (e: any) => {
		if (e.key === 'Enter') {
			this.createTask();
		}
	};

	createTask = async () => {
		if (this.state.taskName !== '') {
			this.setState({ taskName: '', openNew: false });
			await this.props.createTask(this.state.taskName, this.props.taskList.id);
		}
	};

	updateTaskList = async () => {
		const taskList = this.props.taskList;
		taskList.name = this.state.taskListName;
		await this.props.updateTaskList(taskList);
		this.setState({ renameMode: false });
	};

	archiveTaskList = async () => {
		const taskList = this.props.taskList;
		taskList.archived = true;
		await this.props.updateTaskList(taskList);
	};

	deleteTaskList = async () => {
		this.setState({ openSideMenu: false });
		this.props.deleteTaskList(this.props.taskList);
	};

	toggledTask = async (task: Task, value: boolean) => {
		await this.props.toggledTask(task, this.props.taskList.id, value);
	};

	changedDate = async (task: Task, date: Date | null) => {
		this.props.changedDeadline(task, this.props.taskList, date);
	};

	changedUser = async (task: Task, user: User | null) => {
		this.props.changedAssignee(task, this.props.taskList, user);
	};

	render() {
		const completedTasks = this.props.taskList.tasks.filter(task => task.done);
		const openTasks = this.props.taskList.tasks.filter(task => !task.done).sort((a, b) => a.order - b.order);
		return (
			<Draggable
				draggableId={'tasklist-' + this.props.taskList.id.toString()}
				index={this.props.index}
				isDragDisabled={this.props.taskList.archived}>
				{(pr, sn) => (
					<div className="tasklist" ref={pr.innerRef} {...pr.draggableProps} {...pr.dragHandleProps}>
						<div className="tasklist__top">
							{this.state.renameMode && (
								<input
									type="text"
									value={this.state.taskListName}
									ref={this.textInputTaskListRename}
									autoFocus={true}
									onChange={event => this.setState({ taskListName: event.target.value })}
									onKeyDown={this._handleKeyDownTaskListRename}
									className="tasklist__input"
									placeholder="Enter name of this tasklist"
								/>
							)}
							{!this.state.renameMode && <div className="tasklist__name">{this.props.taskList.name}</div>}
							<div
								ref={this.options}
								className="tasklist__icons"
								onClick={() => this.setState({ openSideMenu: !this.state.openSideMenu })}>
								<div className={this.state.openSideMenu ? 'dropdown dropdown--active' : 'dropdown'}>
									<div className="icon icon--more"></div>
									<div className="dropdown__menu">
										<div className="dropdown__content">
											<div className="dropdown__set">
												<div
													className="dropdown__option"
													onClick={() => {
														this.setState({ openSideMenu: false, renameMode: true });
														if (this.textInputTaskListRename.current) {
															this.textInputTaskListRename.current!.focus();
														}
													}}>
													Rename
												</div>
												<div className="dropdown__option" onClick={this.archiveTaskList}>
													Archive
												</div>
												<div className="dropdown__option" onClick={this.deleteTaskList}>
													Delete
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div className="tasks">
							<Droppable droppableId={'tasklist-' + this.props.taskList.id.toString()} type="tasks">
								{(provided, snapshot) => (
									<div ref={provided.innerRef} {...provided.droppableProps}>
										{openTasks.map((task, index) => (
											<Draggable
												draggableId={'task-' + task.id.toString()}
												index={index}
												key={task.id}
												isDragDisabled={this.props.taskList.archived}>
												{(provided, snapshot) => (
													<div {...provided.draggableProps} ref={provided.innerRef} {...provided.dragHandleProps}>
														<TaskComponent
															task={task}
															toggle={this.toggledTask}
															taskListId={this.props.taskList.id}
															changedDate={this.changedDate}
															changedUser={this.changedUser}
															{...this.props}
														/>
													</div>
												)}
											</Draggable>
										))}
										{provided.placeholder}
									</div>
								)}
							</Droppable>

							{(this.state.openNew || this.props.taskList.tasks.length === 0) && (
								<div className="task task--last">
									<div className="task__left">
										<form className="task__checkbox-form">
											<label htmlFor="task" className="task__checkbox">
												<input name="task" type="checkbox" className="h-hidden" />
												<div className="task__checkbox__img"></div>
											</label>
										</form>
										<div className="task__name">
											<input
												type="text"
												name="task name"
												className="task__name__input"
												value={this.state.taskName}
												autoFocus={true}
												ref={this.textInputTaskNew}
												onKeyDown={this._handleKeyDownTaskNew}
												placeholder="Enter task name"
												autoComplete="off"
												onChange={event => this.setState({ taskName: event.target.value })}
											/>
										</div>
									</div>
								</div>
							)}

							<div className="tasks__btns">
								<button
									className={this.state.openNew ? 'btn btn-add btn--add-sec' : 'btn btn--add'}
									onClick={() => {
										this.setState({ openNew: !this.state.openNew, taskName: '' });
										if (this.textInputTaskNew.current) {
											this.textInputTaskNew.current.focus();
										}
									}}>
									Add new task
								</button>
								{completedTasks.length !== 0 && (
									<button className="btn btn--blank" onClick={() => this.setState({ openCompleted: !this.state.openCompleted })}>
										{this.state.openCompleted ? 'Hide' : 'Show'} completed todos
									</button>
								)}
							</div>

							{this.state.openCompleted &&
								completedTasks.map((task, index) => {
									if (this.state.allowedDoneTasks > index) {
										return (
											<TaskComponent
												task={task}
												toggle={this.toggledTask}
												taskListId={this.props.taskList.id}
												key={task.id}
												changedDate={this.changedDate}
												changedUser={this.changedUser}
												{...this.props}
											/>
										);
									} else if (this.state.allowedDoneTasks == index) {
										return (
											<button
												className="btn btn--load-more"
												onClick={() =>
													this.setState({ allowedDoneTasks: this.state.allowedDoneTasks + Utils.DONE_TASK_BATCH })
												}>
												Load more
											</button>
										);
									} else {
										return '';
									}
								})}
						</div>
					</div>
				)}
			</Draggable>
		);
	}
}

export default TaskListComponent;
