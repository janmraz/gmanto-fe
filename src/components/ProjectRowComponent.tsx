import React, { Component, RefObject } from 'react';
import { RouteComponentProps } from 'react-router-dom';
import Project from '../model/Project';
import ModalComponent from './ModalComponent';
import User from '../model/User';
import { projectStore } from '../stores/ProjectsStore';
import { userStore } from '../stores/UsersStore';

interface ProjectRowComponentRow extends RouteComponentProps<any> {
	project: Project;
	archiveProject: (project: Project) => void;
	unarchiveProject: (project: Project) => void;
	deleteProject: (project: Project) => void;
	reloadData: () => void;
}

interface ProjectRowComponentState {
	name: string;
	openOptions: boolean;
	openModal: boolean;
	openRename: boolean;
}

class ProjectRowComponent extends Component<ProjectRowComponentRow, ProjectRowComponentState> {
	constructor(props: ProjectRowComponentRow) {
		super(props);
		this.textInput = React.createRef();
		this.optionsMenu = React.createRef();
		this.state = {
			name: this.props.project.name,
			openOptions: false,
			openModal: false,
			openRename: false,
		};
	}

	textInput: RefObject<HTMLInputElement>;
	optionsMenu: RefObject<HTMLDivElement>;

	componentWillMount(): void {
		document.addEventListener('mousedown', this._handleClick, false);
	}
	componentWillUnmount(): void {
		document.removeEventListener('mousedown', this._handleClick, false);
	}

	_handleClick = (e: any) => {
		if (this.optionsMenu.current && this.optionsMenu.current.contains(e.target)) {
			return;
		}

		this.setState({ openOptions: false });
	};

	_handleKeyDown = (e: any) => {
		if (e.key === 'Enter') {
			e.preventDefault();
			const project = this.props.project;
			project.name = this.state.name;
			projectStore.editProject(project).then(() => {
				this.setState({ openRename: false, openOptions: false });
				this.props.reloadData();
			});
		}
	};

	onClicked = (e: any) => {
		e.preventDefault();
		this.setState({ openOptions: !this.state.openOptions });
	};

	archive = (e: any) => {
		e.preventDefault();
		this.setState({ openOptions: false });
		this.props.archiveProject(this.props.project);
	};

	unarchive = (e: any) => {
		e.preventDefault();
		this.setState({ openOptions: false });
		this.props.unarchiveProject(this.props.project);
	};
	rename = () => {
		this.setState({ openRename: !this.state.openRename, openOptions: false });
		if (this.textInput.current) {
			this.textInput.current.focus();
		}
	};
	share = (e: any) => {
		e.preventDefault();
		this.setState({ openOptions: false, openModal: true });
	};

	delete = (e: any) => {
		e.preventDefault();
		this.setState({ openOptions: false });
		this.props.deleteProject(this.props.project);
	};

	invite = async (email: string) => {
		await projectStore.addCollaborator(this.props.project.id, email);
		await this.props.reloadData();
	};

	changeOwnership = async (user: User) => {
		await projectStore.changeOwner(this.props.project.id, user);
		await this.props.reloadData();
	};

	removeUser = async (user: User) => {
		if (window.confirm('Are you sure you wish to remove this collaborator?')) {
			await projectStore.removeCollaborator(this.props.project.id, user);
			await this.props.reloadData();
		}
	};

	render() {
		return [
			<div key={'modal'}>
				{this.state.openModal && (
					<ModalComponent
						open={this.state.openModal}
						onClose={() => this.setState({ openModal: false })}
						invite={this.invite}
						peopleInProject={this.props.project.collaborators}
						removeUser={this.removeUser}
						makeOwner={this.changeOwnership}
					/>
				)}
			</div>,
			<div className="pbox__left" key={'name'}>
				{this.state.openRename ? (
					<input
						type="text"
						value={this.state.name}
						ref={this.textInput}
						autoFocus={true}
						onChange={event => this.setState({ name: event.target.value })}
						onKeyDown={this._handleKeyDown}
						className="pbox__name"
						placeholder="Enter name of this project"
					/>
				) : (
					<div className="pbox__name">{this.props.project.name}</div>
				)}
			</div>,
			<div className="pbox__right" key={'data'}>
				<div className="pbox__labels">
					<div className="label label--clear-np">{this.props.project.tasksOpen} todos</div>
					<div className="label label--tome">{this.props.project.tasksMine} assigned to me</div>
				</div>
				{this.props.project.collaborators && <div className="pbox__users">{this.props.project.collaborators.length}</div>}
			</div>,
			<div className="pbox__icons" onClick={e => e.preventDefault()} key={'options'} ref={this.optionsMenu}>
				<div className={this.state.openOptions ? 'dropdown dropdown--active' : 'dropdown'}>
					<div className="icon icon--more" onClick={this.onClicked}></div>
					<div className="dropdown__menu">
						<div className="dropdown__content">
							<div className="dropdown__set">
								{this.isOwner() && (
									<div className="dropdown__option" onClick={this.rename}>
										Rename
									</div>
								)}
								{this.isOwner() && (
									<div
										className={this.props.project.archived ? 'dropdown__option dropdown__option--check' : 'dropdown__option'}
										onClick={this.props.project.archived ? this.unarchive : this.archive}>
										{!this.props.project.archived ? 'Archive' : 'Unarchive'}
									</div>
								)}
								{this.isOwner() && (
									<div className="dropdown__option" onClick={this.delete}>
										Delete
									</div>
								)}
								<div className="dropdown__option" onClick={this.share}>
									Share
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>,
		];
	}

	isOwner = () => {
		return userStore.user && this.props.project.collaborators.find(user => user.user.id === userStore.user!.id && user.owner);
	};
}

export default ProjectRowComponent;
