import React, {Component, ComponentProps, RefObject} from 'react';
import User from '../model/User';
import Task from '../model/Task';
import UserPhoto from './UserPhotoComponent';
import ProjectToUser from '../model/ProjectToUser';

interface DropdownFollowersComponentProps extends ComponentProps<any> {
	peopleInProject: ProjectToUser[];
	task: Task;
	addUser: (user: User) => void;
	removeUser: (user: User) => void;
	inviteUser: () => void;
}

interface DropdownFollowersComponentState {
	search: string;
	openUsers: boolean;
}

class DropdownFollowersComponent extends Component<DropdownFollowersComponentProps, DropdownFollowersComponentState> {
	constructor(props: DropdownFollowersComponentProps) {
		super(props);
		this.usersOptions = React.createRef();
		this.state = {
			search: '',
			openUsers: false,
		};
	}
	usersOptions: RefObject<HTMLDivElement>;

	componentWillMount(): void {
		document.addEventListener('mousedown', this._handleClick, false);

	}
	componentWillUnmount(): void {
		document.removeEventListener('mousedown', this._handleClick, false);
	}

	_handleClick = (e: any) => {
		if(this.usersOptions.current && this.usersOptions.current.contains(e.target)){
			return
		}

		this.setState({openUsers: false});
	};

	clickedUser = async (user: User) => {
		if (this.props.task.followers.find(f => f.id === user.id)) {
			this.props.removeUser(user);
		} else {
			this.props.addUser(user);
		}
	};

	render() {
		const filteredUsers = this.props.peopleInProject.filter(
			user =>
				user.user.firstName.toLocaleLowerCase().includes(this.state.search.toLocaleLowerCase()) ||
				user.user.lastName.toLocaleLowerCase().includes(this.state.search.toLocaleLowerCase()) ||
				user.user.email.toLocaleLowerCase().includes(this.state.search.toLocaleLowerCase())
		);
		return (
			<div ref={this.usersOptions} key={'dropdown'} className="task__user" onClick={() => this.setState({ openUsers: !this.state.openUsers })}>
				<div className={this.state.openUsers ? 'dropdown dropdown--active' : 'dropdown'}>
					<div className="circles task__user">
						{this.props.task.followers.length !== 0 && (
							<div className="circle">
								<UserPhoto user={this.props.task.followers[0]} />
							</div>
						)}
						<div className="circle">
							<div className="circle circle--add-user"></div>
						</div>
					</div>
					<div className="dropdown__menu dropdown__menu--task" onClick={e => e.stopPropagation()}>
						<div className="dropdown__content">
							<div className="dropdown__set dropdown__set--paddings">
								<div className="dropdown__item dropdown__item--top">
									<input
										name="search"
										placeholder="Search and assign"
										type="text"
										className="form-input"
										value={this.state.search}
										onChange={e => this.setState({ search: e.target.value })}
									/>
								</div>
							</div>
							<div className="dropdown__set">
								{filteredUsers.map(user => (
									<div
										key={user.id}
										className={
											!!this.props.task.followers.find(f => f.id === user.user.id)
												? 'dropdown__user dropdown__user--check'
												: 'dropdown__user dropdown'
										}
										onClick={() => this.clickedUser(user.user)}>
										<div className="dropdown__user__circle">
											<div className="circles">
												<div className="circle">
													<UserPhoto user={user.user} />
												</div>
											</div>
										</div>
										<div className="dropdown__user__name">
											{user.user.firstName === '' && user.user.lastName === ''
												? user.user.email
												: user.user.firstName + ' ' + user.user.lastName}
										</div>
									</div>
								))}
								<div className="dropdown__item dropdown__item--last">
									<button className="btn btn--add-light" onClick={() => this.props.inviteUser()}>
										Invite new user
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default DropdownFollowersComponent;
