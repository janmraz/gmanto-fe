import React, { Component, ComponentProps } from 'react';
import Comment from '../model/Comment';
import UserPhoto from './UserPhotoComponent';
import { Link } from 'react-router-dom';
const { DateTime } = require('luxon');

interface NotificationsProps extends ComponentProps<any> {
	projectId: number;
	onOpen: () => void;
	notifications: Comment[];
	onClickMore: () => void;
}

interface NotificationComponentState {
	open: boolean;
	showAll: boolean;
	scrollPosition: number;
	currentNotificationsNumber: number;
}

class NotificationComponent extends Component<NotificationsProps, NotificationComponentState> {
	constructor(props: NotificationsProps) {
		super(props);
		this.state = {
			open: false,
			showAll: false,
			scrollPosition: window.pageYOffset,
			currentNotificationsNumber: 5,
		};
	}

	async componentDidMount() {
		window.addEventListener('scroll', this.handleScroll);
	}

	componentWillUnmount() {
		window.removeEventListener('scroll', this.handleScroll);
	}

	handleScroll = () => {
		this.setState({
			scrollPosition: window.pageYOffset,
		});
	};
	onToggle = async () => {
		if (!this.state.open) {
			this.props.onOpen();
		}
		this.setState({ open: !this.state.open });
	};

	render() {
		const unreadNotifications = !this.props.notifications.every(not => not.seen);
		return (
			<div>
				<div className="sidebar-switch" onClick={this.onToggle}>
					<div className={unreadNotifications ? 'sidebar-switch__icon sidebar-switch__icon--new' : ''}>
						<div className="sidebar-switch__icon__img" title="Notifications"></div>
					</div>
				</div>

				<div className={this.state.open ? 'sidebar sidebar--show' : 'sidebar'}>
					<div className="sidebar__top">
						<div className="sidebar__headline">Notifications</div>
						<div className={this.state.showAll ? 'switch switch--on' : 'switch'}>
							<div className="switch__content switch__content--left" onClick={() => this.setState({ showAll: !this.state.showAll })}>
								<div className="switch__text">Show all notifications</div>
								<div className="switch__box">
									<div className="switch__box__in"></div>
								</div>
							</div>
						</div>
					</div>
					<div className="sidebar__content">
						<div className="sdivider">Unread</div>
						<div className="notifications">
							{this.props.notifications
								.filter(n => n.infoComment || this.state.showAll)
								.map(notification => {
									// @ts-ignore
									const url = '/project-' + this.props.projectId + '/task-' + notification.task.id + '/reload';
									return (
										<Link className="notification" key={notification.id} to={url} onClick={() => this.forceUpdate}>
											<div className="notification__left">
												<div className="circles">
													<div className="circle circle--small">
														<UserPhoto user={notification.creator} />
													</div>
												</div>
											</div>
											<div className="notification__right">
												{notification.infoComment && <div className="notification__text">{notification.text}</div>}
												{!notification.infoComment && (
													<span>
														<div className="notification__text">
															{notification.creator.firstName + ' ' + notification.creator.lastName + ' has commented'}
														</div>
														<div
															className="notification__comment"
															dangerouslySetInnerHTML={{ __html: notification.text }}
														/>
													</span>
												)}
												<div className="notification__date">{DateTime.fromISO(notification.dateCreated).toRelative()}</div>
											</div>
										</Link>
									);
								})}
						</div>
						<button className="btn btn--load-more" onClick={this.props.onClickMore}>
							Load more
						</button>
					</div>
				</div>
				<div
					className={this.state.scrollPosition > 200 ? 'top-arrow top-arrow--on' : 'top-arrow'}
					onClick={() => window.scrollTo(0, 0)}></div>
			</div>
		);
	}
}

export default NotificationComponent;
