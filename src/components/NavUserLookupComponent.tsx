import React, { Component, ComponentProps } from 'react';
import UserPhoto from './UserPhotoComponent';
import ProjectToUser from "../model/ProjectToUser";

interface NavUserLookupComponentProps extends ComponentProps<any> {
	peopleInProject: ProjectToUser[];
}

class NavUserLookupComponent extends Component<NavUserLookupComponentProps, {}> {
	render() {
		if (this.props.peopleInProject.length < 2) return <div></div>;
		return (
			<div className="circles tabs__users">
				<div className="circle">
					<UserPhoto user={this.props.peopleInProject[0].user} />
				</div>
				<div className="circle">
					<UserPhoto user={this.props.peopleInProject[1].user} />
				</div>
				{this.props.peopleInProject.length > 2 && (
					<div className="circle">
						<div className="circle__text" title="User name">
							<div className="circle__text__inicials">+{this.props.peopleInProject.length - 2}</div>
						</div>
					</div>
				)}
			</div>
		);
	}
}

export default NavUserLookupComponent;
