import React, { Component } from 'react';
import Task from '../model/Task';
import { Link, RouteComponentProps } from 'react-router-dom';
import DatePicker from 'react-datepicker';
import User from '../model/User';
import DropdownUsersComponent from './DropdownUsersComponent';
import ProjectToUser from '../model/ProjectToUser';
import Utils from '../Utils';

interface TaskProps extends RouteComponentProps<any> {
	task: Task;
	taskListId: number;
	peopleInProject: ProjectToUser[];
	toggle: (task: Task, value: boolean) => void;
	changedDate: (task: Task, date: Date | null) => void;
	changedUser: (task: Task, user: User | null) => void;
	inviteUser: () => void;
}

interface TaskComponentState {
	isChecked: boolean;
	openUsers: boolean;
	search: string;
}

class TaskComponent extends Component<TaskProps, TaskComponentState> {
	constructor(props: TaskProps) {
		super(props);
		this.state = {
			isChecked: this.props.task.done,
			openUsers: false,
			search: '',
		};
	}
	onChecked = async () => {
		await this.props.toggle(this.props.task, !this.state.isChecked);
		this.setState({ isChecked: !this.state.isChecked });
	};

	onChangeUser = async (user: User | null) => {
		await this.props.changedUser(this.props.task, user);
		this.setState({ openUsers: false });
	};

	render() {
		const unreadComments = !this.props.task.comments.filter(c => !c.infoComment).every(c => c.seen);
		return (
			<div className="task">
				<div className="task__left">
					<form className="task__checkbox-form">
						<label htmlFor="task" className="task__checkbox">
							<input name="task" type="checkbox" className="h-hidden" />
							<div
								className={this.state.isChecked ? 'task__checkbox__img task__checkbox__img--done' : 'task__checkbox__img'}
								onClick={this.onChecked}></div>
						</label>
					</form>
					<div className="task__name">
						<Link
							onClick={event => (this.props.task.id === Utils.VOID_ID ? event.preventDefault() : () => ({}))}
							to={this.props.location.pathname + '/task-' + this.props.task.id}
							className="task__name__text">
							{this.props.task.name}
						</Link>
					</div>
				</div>
				<div className="task__right">
					<DropdownUsersComponent
						task={this.props.task}
						inviteUser={this.props.inviteUser}
						peopleInProject={this.props.peopleInProject}
						onChangeUser={this.onChangeUser}
					/>
					<div
						className={
							this.props.task.dateDue && Utils.isDateBeforeToday(new Date(this.props.task.dateDue)) ? 'task__date task__date--due' : 'task__date'
						}>
						<DatePicker
							selected={this.props.task.dateDue ? new Date(this.props.task.dateDue) : null}
							onChange={date => {
								if (date && Utils.isToday(date!)) {
									return this.props.changedDate(this.props.task, null);
								}
								this.props.changedDate(this.props.task, date);
							}}
							todayButton={'No due date'}
							placeholderText="Set Deadline"
						/>
					</div>
					<div className={unreadComments ? 'task__comments task__comments--new' : 'task__comments'}>
						{this.props.task.comments && this.props.task.comments.filter(c => !c.infoComment).length}
					</div>
				</div>
			</div>
		);
	}
}

export default TaskComponent;
