import React, { Component, ComponentProps } from 'react';
import User from '../model/User';

interface ModalProps extends ComponentProps<any> {
	user: User;
}

class UserPhoto extends Component<ModalProps, {}> {
	render() {
		if (!this.props.user) return <span />;
		if (this.props.user.profileImgUrl) {
			return <img src={this.props.user.profileImgUrl} alt="User name" title="User name" />;
		} else if (this.props.user.firstName && this.props.user.lastName && this.props.user.firstName !== '' && this.props.user.lastName !== '') {
			return (
				<div className="circle__text" title="User name">
					<div className="circle__text__inicials">{(this.props.user.firstName[0] + this.props.user.lastName[0]).toUpperCase()}</div>
				</div>
			);
		} else {
			return <span />;
		}
	}
}

export default UserPhoto;
