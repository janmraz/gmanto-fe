import React, { Component, ComponentProps, RefObject } from 'react';
import { userStore } from '../stores/UsersStore';
import { NavLink } from 'react-router-dom';
import NavUserLookupComponent from './NavUserLookupComponent';
import UserPhoto from './UserPhotoComponent';
import ProjectToUser from '../model/ProjectToUser';

interface NavProps extends ComponentProps<any> {
	projectsNav: boolean;
	peopleInProject: ProjectToUser[];
	onShare: () => void;
	history: any;
}

interface NavState {
	openDropdown: boolean;
	darkMode: boolean;
}

class Nav extends Component<NavProps, NavState> {
	async componentDidMount() {
		await this.reloadData();
	}

	 reloadData = async () => {
		try {
			await userStore.getProfile();
			this.setState({darkMode: userStore!.user!.darkMode});
			document.body.className = userStore!.user!.darkMode ? 'darkmode' : '';
		} catch (e) {
			console.log(e);
			if (e && e.response && (e.response.status === 403 || e.response.status === 401)) {
				this.props.history.push('/welcome');
			}
		}
	};

	logout = async () => {
		try {
			await userStore.logout();
		} catch (e) {
			console.log(e);
		}
		localStorage.clear();
		this.props.history.push('/welcome');
	};

	constructor(props: ComponentProps<any>) {
		super(props);
		this.userOptions = React.createRef();
		this.state = {
			openDropdown: false,
			darkMode: false,
		};
	}

	changeDarkModeSettings = async () => {
		const user = userStore.user!;
		user.darkMode = !this.state.darkMode;
		this.setState({darkMode: !this.state.darkMode});

		await userStore.updateUser(user);
		await this.reloadData();
	};

	userOptions: RefObject<HTMLDivElement>;

	componentWillMount(): void {
		document.addEventListener('mousedown', this._handleClick, false);
	}
	componentWillUnmount(): void {
		document.removeEventListener('mousedown', this._handleClick, false);
	}

	_handleClick = (e: any) => {
		if (this.userOptions.current && this.userOptions.current.contains(e.target)) {
			return;
		}

		this.setState({ openDropdown: false });
	};

	render() {
		return (
			<nav className="header">
				<NavLink to="/" className="header__logo">
					<div className="header__logo__img"></div>
				</NavLink>
				<small>
					You are running this application in <b>{process.env.NODE_ENV}</b> mode.
				</small>
				<div className="header__right">
					{!this.props.projectsNav && <NavUserLookupComponent peopleInProject={this.props.peopleInProject} />}
					{!this.props.projectsNav && (
						<button className="btn btn--invite" onClick={this.props.onShare}>
							Share project
						</button>
					)}

					<div
						className={this.state.openDropdown ? 'dropdown header__avatar dropdown--active' : 'dropdown header__avatar'}
						onClick={() => this.setState({ openDropdown: !this.state.openDropdown })}
						ref={this.userOptions}>
						<div className="circles">
							<div className="circle circle--big header__user">
								<UserPhoto user={userStore.user!} />
							</div>
						</div>
						<div className="dropdown__menu">
							<div className="dropdown__content">
								<div className="dropdown__head">{userStore.user && userStore.user.firstName + ' ' + userStore.user.lastName}</div>
								<div className="dropdown__set">
									<NavLink className="dropdown__option" to={'/settings'}>
										My profile settings
									</NavLink>
									<div
										className={this.state.darkMode ? 'dropdown__option dropdown__option--check' : 'dropdown__option'}
										onClick={this.changeDarkModeSettings}>
										Darkmode
									</div>
									<NavLink className="dropdown__option" to={'/welcome'}>
										Help
									</NavLink>
									<div className="dropdown__option" onClick={this.logout}>
										Log out
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</nav>
		);
	}
}

export default Nav;
