import React, { Component, ComponentProps } from 'react';
import User from '../model/User';
import ModalRowComponent from './ModalRowComponent';
import ProjectToUser from '../model/ProjectToUser';
import {userStore} from "../stores/UsersStore";

interface ModalProps extends ComponentProps<any> {
	open: boolean;
	onClose: () => void;
	invite: (email: string) => void;
	peopleInProject: ProjectToUser[];
	removeUser: (user: User) => void;
	makeOwner: (user: User) => void;
}

interface ModalComponentState {
	email: string;
}

class ModalComponent extends Component<ModalProps, ModalComponentState> {
	constructor(props: ModalProps) {
		super(props);
		this.state = {
			email: '',
		};
	}

	render() {
		return (
			<div className={this.props.open ? 'lightbox lightbox--show' : 'lightbox'} onClick={e => e.preventDefault()}>
				<div className="lightbox__background"></div>
				<div className="lightbox__wrapper">
					<div className="lightbox__box">
						<div className="lightbox__close" onClick={this.props.onClose}></div>
						<div className="lightbox__headline">Share this project</div>
						<div className="lightbox__invite">
							<div className="form-group">
								<label htmlFor="email" className="form-label">
									Invite more collaborators via e-mail address
								</label>
								<input
									className="form-input"
									name="email"
									placeholder="E-mail"
									type="text"
									value={this.state.email}
									onChange={event => this.setState({ email: event.target.value })}
								/>
							</div>
							<div className="form-group">
								{this.state.email !== '' && (
									<button
										className="btn"
										type="submit"
										onClick={() => {
											this.setState({ email: '' });
											this.props.invite(this.state.email);
										}}>
										Send invitation
									</button>
								)}
							</div>
						</div>

						<div className="lightbox__headline">Collaborators</div>
						<div className="lightbox__content">
							{this.props.peopleInProject.map(user => (
								<ModalRowComponent
									key={user.id}
									isOwner={!!this.props.peopleInProject.find(c => c.user.id === userStore.user!.id && c.owner)}
									projectToUser={user}
									removeUser={this.props.removeUser}
									makeOwner={this.props.makeOwner}
								/>
							))}
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default ModalComponent;
