import React, { Component, ComponentProps } from 'react';
import Comment from '../model/Comment';
import { userStore } from '../stores/UsersStore';
import UserPhoto from './UserPhotoComponent';
import { MergeTags, TrixEditor } from 'react-trix';
import Configuration from '../Configuration';
const { DateTime } = require('luxon');

interface ModalProps extends ComponentProps<any> {
	comment: Comment;
	deleteComment: (commentId: number) => void;
	taskListId: number;
	taskId: number;
	mergeTags: MergeTags[];
	editComment: (comment: string, commentId: number) => void;
}

interface ModalComponentState {
	openOptions: boolean;
	openEdit: boolean;
	comment: string;
	editor: any | null;
}

class CommentLineComponent extends Component<ModalProps, ModalComponentState> {
	constructor(props: ModalProps) {
		super(props);
		this.state = {
			openOptions: false,
			openEdit: false,
			comment: this.props.comment.text,
			editor: null,
		};
	}
	interval: any;

	componentDidMount() {
		this.interval = setInterval(() => this.forceUpdate(), 60000);
	}
	componentWillUnmount() {
		if (this.interval) {
			clearInterval(this.interval);
		}
	}

	handleEditorReady = (editor: any) => {
		this.setState({ editor });
	};

	commentChanged = (html: string, text: string) => {
		if (this.state) {
			this.setState({ comment: html });
		}
	};

	send = () => {
		this.setState({openEdit: false});
		this.props.editComment(this.state.comment, this.props.comment.id);
	};

	render() {
		if (this.props.comment.infoComment) {
			return (
				<div className="inforow">
					<div className="inforow__left">
						<div className="circles">
							<div className="circle">
								<UserPhoto user={this.props.comment.creator} />
							</div>
						</div>
					</div>
					<div className="inforow__right">
						<div className="inforow__text">{this.props.comment.text}</div>
						<div className="inforow__date">{DateTime.fromISO(this.props.comment.dateCreated).toRelative()}</div>
					</div>
				</div>
			);
		}
		return (
			<div className="comment">
				<div className="comment__left">
					<div className="circles">
						<div className="circle">
							<UserPhoto user={this.props.comment.creator} />
						</div>
					</div>
				</div>
				{!this.state.openEdit ? (
					<div className="comment__right">
						<div className="comment__content">
							<div className="comment__user">{this.props.comment.creator.firstName + '' + this.props.comment.creator.lastName}</div>
							<div className="comment__text" dangerouslySetInnerHTML={{ __html: this.props.comment.text }} />
						</div>
						<div className="comment__info">
							<div className="comment__date">{DateTime.fromISO(this.props.comment.dateCreated).toRelative()}</div>
						</div>
					</div>
				) : (
					<div className="comment__right">
						<div className="texta">
							<TrixEditor
								autoFocus={true}
								placeholder="Write a comment or upload a file"
								onChange={(html: string, text: string) => this.commentChanged(html, text)}
								onEditorReady={this.handleEditorReady}
								value={this.state.comment}
								mergeTags={this.props.mergeTags}
								uploadURL={
									Configuration.API_URL + '/taskLists/' + this.props.taskListId + '/tasks/' + this.props.taskId + '/upload-file'
								}
							/>

							<div className="texta__control">
								<div className="texta__control__left">
									<div className="texta__icon texta__icon--b"></div>
									<div className="texta__icon texta__icon--i"></div>
									<div className="texta__icon texta__icon--u"></div>
									<div className="texta__icon texta__icon--ul"></div>
									<div className="texta__icon texta__icon--ol"></div>
								</div>
								<div className="texta__control__right">
									<div className="texta__icon texta__icon--file"></div>
									<button className="btn" onClick={this.send}>
										Send
									</button>
								</div>
							</div>
						</div>
					</div>
				)}
				{this.props.comment.creator.id === userStore.user!.id && (
					<div className="comment__icons" onClick={() => this.setState({ openOptions: !this.state.openOptions })}>
						<div className={this.state.openOptions ? 'dropdown dropdown--active' : 'dropdown'}>
							<div className="icon icon--more"></div>
							<div className="dropdown__menu">
								<div className="dropdown__content">
									<div className="dropdown__set">
										<div className="dropdown__option" onClick={() => this.setState({ openEdit: !this.state.openEdit })}>
											Edit
										</div>
										<div className="dropdown__option" onClick={() => this.props.deleteComment(this.props.comment.id)}>
											Delete
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				)}
			</div>
		);
	}
}

export default CommentLineComponent;
