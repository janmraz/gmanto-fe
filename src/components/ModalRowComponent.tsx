import React, { Component, ComponentProps } from 'react';
import User from '../model/User';
import UserPhoto from './UserPhotoComponent';
import ProjectToUser from '../model/ProjectToUser';

interface ModalRowComponentRow extends ComponentProps<any> {
	projectToUser: ProjectToUser;
	isOwner: boolean;
	removeUser: (user: User) => void;
	makeOwner: (user: User) => void;
}

interface ModalRowComponentState {
	open: boolean;
}

class ModalRowComponent extends Component<ModalRowComponentRow, ModalRowComponentState> {
	constructor(props: ModalRowComponentRow) {
		super(props);
		this.state = {
			open: false,
		};
	}
	onClick = () => {
		if (this.props.projectToUser.owner && !this.props.isOwner) return;
		this.setState({ open: !this.state.open });
	};
	render() {
		return (
			<div className="person">
				<div className="person__left">
					<div className="circles">
						<div className="circle">
							<UserPhoto user={this.props.projectToUser.user} />
						</div>
					</div>
					<div className="person__name">
						{this.props.projectToUser.user.lastName !== '' && this.props.projectToUser.user.firstName !== ''
							? this.props.projectToUser.user.firstName +
							  ' ' +
							  this.props.projectToUser.user.lastName +
							  '(' +
							  this.props.projectToUser.user.email +
							  ')'
							: this.props.projectToUser.user.email}
					</div>
				</div>
				<div className="person__right">
					<div className="person__role">{this.props.projectToUser.owner ? 'Owner' : 'Collaborator'}</div>
					<div className="person__icons" onClick={this.onClick}>
						<div className={this.state.open ? 'dropdown dropdown--active' : 'dropdown'}>
							<div className="icon icon--more"></div>
							{!this.props.projectToUser.owner && <div className="dropdown__menu">
								<div className="dropdown__content">
									<div className="dropdown__set">
										{this.props.isOwner && (
											<div
												className='dropdown__option'
												onClick={() =>
													this.props.projectToUser.owner
														? () => this.setState({ open: false })
														: this.props.makeOwner(this.props.projectToUser.user)
												}>
												Set Owner
											</div>
										)}
											<div className="dropdown__option" onClick={() => this.props.removeUser(this.props.projectToUser.user)}>
												Remove
											</div>
									</div>
								</div>
							</div>}
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default ModalRowComponent;
