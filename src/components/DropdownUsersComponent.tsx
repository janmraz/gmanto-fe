import React, { Component, ComponentProps, RefObject } from 'react';
import User from '../model/User';
import Task from '../model/Task';
import UserPhoto from './UserPhotoComponent';
import ProjectToUser from '../model/ProjectToUser';

interface DropdownUsersComponentProps extends ComponentProps<any> {
	peopleInProject: ProjectToUser[];
	task: Task;
	onChangeUser: (user: User | null) => void;
	inviteUser: () => void;
}

interface DropdownUsersComponentState {
	search: string;
	openUsers: boolean;
}

class DropdownUsersComponent extends Component<DropdownUsersComponentProps, DropdownUsersComponentState> {
	constructor(props: DropdownUsersComponentProps) {
		super(props);
		this.usersOptions = React.createRef();
		this.state = {
			search: '',
			openUsers: false,
		};
	}

	clickedUser = async (user: User | null) => {
		this.props.onChangeUser(user);
		this.setState({ openUsers: false });
	};

	usersOptions: RefObject<HTMLDivElement>;

	componentWillMount(): void {
		document.addEventListener('mousedown', this._handleClick, false);
	}
	componentWillUnmount(): void {
		document.removeEventListener('mousedown', this._handleClick, false);
	}

	_handleClick = (e: any) => {
		if (this.usersOptions.current && this.usersOptions.current.contains(e.target)) {
			return;
		}

		this.setState({ openUsers: false });
	};

	render() {
		const filteredUsers = this.props.peopleInProject.filter(
			user =>
				user.user.firstName.toLocaleLowerCase().includes(this.state.search.toLocaleLowerCase()) ||
				user.user.lastName.toLocaleLowerCase().includes(this.state.search.toLocaleLowerCase()) ||
				user.user.email.toLocaleLowerCase().includes(this.state.search.toLocaleLowerCase())
		);
		return [
			<div key={'dropdown'} className="task__user" onClick={() => this.setState({ openUsers: !this.state.openUsers })}>
				<div ref={this.usersOptions} className={this.state.openUsers ? 'dropdown dropdown--active' : 'dropdown'}>
					<div className="circles">
						<div className="circle">
							{this.props.task.assignee ? (
								<UserPhoto user={this.props.task.assignee} />
							) : (
								<div className="circle circle--add-user"></div>
							)}
						</div>
					</div>
					<div className="dropdown__menu dropdown__menu--task" onClick={e => e.stopPropagation()}>
						<div className="dropdown__content">
							<div className="dropdown__set dropdown__set--paddings">
								<div className="dropdown__item dropdown__item--top">
									<input
										name="search"
										placeholder="Search and assign"
										type="text"
										className="form-input"
										value={this.state.search}
										onChange={e => this.setState({ search: e.target.value })}
									/>
								</div>
							</div>
							<div className="dropdown__set">
								<div
									className={this.props.task.assignee === null ? 'dropdown__user dropdown__user--check' : 'dropdown__user dropdown'}
									onClick={() => this.clickedUser(null)}>
									<div className="dropdown__user__name">Unassigned</div>
								</div>
								{filteredUsers.map(user => (
									<div
										key={user.id}
										className={
											this.props.task.assignee && this.props.task.assignee.id === user.user.id
												? 'dropdown__user dropdown__user--check'
												: 'dropdown__user dropdown'
										}
										onClick={() => this.clickedUser(user.user)}>
										<div className="dropdown__user__circle">
											<div className="circles">
												<div className="circle">
													<UserPhoto user={user.user} />
												</div>
											</div>
										</div>
										<div className="dropdown__user__name">
											{user.user.firstName === '' && user.user.lastName === ''
												? user.user.email
												: user.user.firstName + ' ' + user.user.lastName}
										</div>
									</div>
								))}
								<div className="dropdown__item dropdown__item--last">
									<button className="btn btn--add-light" onClick={() => this.props.inviteUser()}>
										Invite new user
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>,
			<div key={'name'} className="task__user-text" onClick={() => this.setState({ openUsers: !this.state.openUsers })}>
				{this.props.task.assignee &&
					(this.props.task.assignee.firstName === '' && this.props.task.assignee.lastName === ''
						? this.props.task.assignee.email
						: this.props.task.assignee.firstName + ' ' + this.props.task.assignee.lastName[0] + '.')}
			</div>,
		];
	}
}

export default DropdownUsersComponent;
